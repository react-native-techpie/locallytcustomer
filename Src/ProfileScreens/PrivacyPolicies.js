import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import Icon from "react-native-vector-icons/FontAwesome5";

const { height, width } = Dimensions.get("window");

export default function PrivacyPolicies({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 20,
        }}
      >
        <View
          style={{
            width: "60%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent: "flex-end",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
          flex: 1,
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              width: width,
              alignItems: "center",
            }}
          >
            <View
              style={{
                width: "90%",
                marginTop: 15,
                padding: 0,
                height: "100%",
                justifyContent: "space-around",
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: 19,
                  color: "#253f67",
                  marginLeft: 12,
                  lineHeight: 20,
                }}
              >
                Privacy Policies
              </Text>

              <Text
                style={{
                  fontFamily: "Helvetica",
                  fontSize: 14,
                  color: "#253f67",
                  marginLeft: 12,
                  lineHeight: 20,
                  marginTop: 20,
                }}
              >
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </Text>
              <Text
                style={{
                  fontFamily: "Helvetica",
                  fontSize: 14,
                  color: "#253f67",
                  marginLeft: 12,
                  lineHeight: 20,
                  marginTop: 20,
                }}
              >
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </Text>
              <Text
                style={{
                  fontFamily: "Helvetica",
                  fontSize: 14,
                  color: "#253f67",
                  marginLeft: 12,
                  lineHeight: 20,
                  marginTop: 20,
                }}
              >
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </Text>
              <Text
                style={{
                  fontFamily: "Helvetica",
                  fontSize: 14,
                  color: "#253f67",
                  marginLeft: 12,
                  lineHeight: 20,
                  marginTop: 20,
                }}
              >
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({});
