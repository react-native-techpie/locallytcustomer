import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import Icon from "react-native-vector-icons/FontAwesome5";

const { height, width } = Dimensions.get("window");

export default function UpdateEmail({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 20,
        }}
      >
        <View
          style={{
            width: "60%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent: "flex-end",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          height: "50%",
          borderRadius: 25,
        }}
      >
        <ScrollView
          style={{ height: "100%", flex: 1 }}
          showsVerticalScrollIndicator={false}
        >
          <View
            style={{
              width: width,
              alignItems: "center",
              justifyContent: "center",
              height: "100%",
            }}
          >
            <View
              style={{
                width: "90%",
                marginTop: 15,
                justifyContent: "center",
                              height: "100%",
                alignItems:'center'
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: 19,
                  color: "#253f67",
                  marginLeft: 12,
                                  lineHeight: 20,
                  marginTop:55,
                  textAlign:'center'
                }}
              >
                Update Your Email
              </Text>
              <View
                style={{
                  width: "90%",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 40,
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: 16,
                    color: "#253f67",
                    marginLeft: 12,
                  }}
                >
                  Your Email
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 19,
                    color: "#253f67",
                    marginLeft: 12,
                    lineHeight: 20,
                  }}
                >
                  Rakesh@gmail.com
                </Text>
              </View>
              <View
                style={{ width: "100%", alignItems: "center", marginTop: 40 }}
              >
                <TouchableOpacity>
                  <View
                    style={{
                      height: 40,
                      width: width - 190,
                      backgroundColor: "#fb5414",
                      borderRadius: 39,
                      justifyContent: "center",
                      marginTop: 25,
                    }}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontFamily: "Helvetica-Bold",
                        textAlign: "center",
                        fontSize: 17,
                      }}
                    >
                      + Add New
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({});
