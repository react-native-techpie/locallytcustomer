import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Modal,
  TouchableWithoutFeedback,
  Alert,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import { Dropdown } from "react-native-material-dropdown-v2";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
  AccordionList,
} from "accordion-collapse-react-native";

const { height, width } = Dimensions.get("window");

export default function ShareList({ navigation }) {
  const [modalOpen, setModalOpen] = useState(false);

  const [counter, setCounter] = useState(1);

  const increment = () => {
    setCounter(counter + 1);
  };
  const decrement = () => {
    if (counter > 0) {
      setCounter(counter - 1);
    }
  };

  const [counter2, setCounter2] = useState(1);

  const increment2 = () => {
    setCounter2(counter2 + 1);
  };
  const decrement2 = () => {
    if (counter2 > 0) {
      setCounter2(counter2 - 1);
    }
  };

  const [counter3, setCounter3] = useState(1);

  const increment3 = () => {
    setCounter3(counter3 + 1);
  };
  const decrement3 = () => {
    if (counter3 > 0) {
      setCounter3(counter3 - 1);
    }
  };



const [counter4, setCounter4] = useState(1);

const increment4 = () => {
  setCounter4(counter4 + 1);
};
const decrement4 = () => {
  if (counter4 > 0) {
    setCounter4(counter4 - 1);
  }
};

  const [selectedValue4, setSelectedValue4] = useState("Nos");
  



  const [counter5, setCounter5] = useState(1);

  const increment5 = () => {
    setCounter5(counter5 + 1);
  };
  const decrement5 = () => {
    if (counter5 > 0) {
      setCounter5(counter5 - 1);
    }
  };

  const [selectedValue5, setSelectedValue5] = useState("Nos");



  const [counter6, setCounter6] = useState(1);

  const increment6 = () => {
    setCounter6(counter6 + 1);
  };
  const decrement6 = () => {
    if (counter6 > 0) {
      setCounter6(counter6 - 1);
    }
  };

  const [selectedValue6, setSelectedValue6] = useState("Nos");


  const [counter7, setCounter7] = useState(1);

  const increment7 = () => {
    setCounter7(counter7 + 1);
  };
  const decrement7 = () => {
    if (counter7 > 0) {
      setCounter7(counter7 - 1);
    }
  };

  const [selectedValue7, setSelectedValue7] = useState("Nos");


const [counter8, setCounter8] = useState(1);

const increment8 = () => {
  setCounter8(counter8 + 1);
};
const decrement8 = () => {
  if (counter8 > 0) {
    setCounter8(counter8 - 1);
  }
};

  const [selectedValue8, setSelectedValue8] = useState("Nos");
  

  const [counter9, setCounter9] = useState(1);

  const increment9 = () => {
    setCounter9(counter9 + 1);
  };
  const decrement9 = () => {
    if (counter9 > 0) {
      setCounter9(counter9 - 1);
    }
  };

  const [selectedValue9, setSelectedValue9] = useState("Nos");


  const [counter10, setCounter10] = useState(1);

  const increment10 = () => {
    setCounter10(counter10 + 1);
  };
  const decrement10 = () => {
    if (counter10 > 0) {
      setCounter10(counter10 - 1);
    }
  };

  const [selectedValue10, setSelectedValue10] = useState("Nos");


  const [counter11, setCounter11] = useState(1);

  const increment11 = () => {
    setCounter11(counter11 + 1);
  };
  const decrement11 = () => {
    if (counter11 > 0) {
      setCounter11(counter11 - 1);
    }
  };

  const [selectedValue11, setSelectedValue11] = useState("Nos");




  const [counter12, setCounter12] = useState(1);

  const increment12 = () => {
    setCounter12(counter12 + 1);
  };
  const decrement12 = () => {
    if (counter12 > 0) {
      setCounter12(counter12 - 1);
    }
  };

  const [selectedValue12, setSelectedValue12] = useState("Nos");


const [counter13, setCounter13] = useState(1);

const increment13 = () => {
  setCounter13(counter13 + 1);
};
const decrement13 = () => {
  if (counter13 > 0) {
    setCounter13(counter13 - 1);
  }
};

const [selectedValue13, setSelectedValue13] = useState("Nos");

  const [selectedValue, setSelectedValue] = useState("Kg");
  const [selectedValue2, setSelectedValue2] = useState("Lit");
  const [selectedValue3, setSelectedValue3] = useState("Nos");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <Modal
        // visible={true}
        visible={modalOpen}
        transparent={true}
        animationType="slide"
      >
        <TouchableWithoutFeedback onPress={() => setModalOpen(false)}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#000000aa",
              width: width,
              alignItems: "center",
              justifyContent: "flex-end",
            }}
          >
            <View
              style={{
                height: 170,
                width: width,
                backgroundColor: "white",
                alignItems: "center",
                justifyContent: "space-around",
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  color: "#283673",
                  marginTop: 0,
                  letterSpacing: 0,
                  fontFamily: "Helvetica-Bold",
                  fontSize: 19,
                }}
              >
                Select Photo To Upload
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  width: "80%",
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: 0,
                }}
              >
                <View
                  style={{
                    width: "50%",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <TouchableOpacity>
                    <Image
                      source={require("../../LocallyTImages/Gallery.png")}
                      style={{ height: 80, width: 80 }}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#283673",
                      marginTop: 0,
                      letterSpacing: 0,
                      fontFamily: "Helvetica-Bold",
                      fontSize: 13,
                    }}
                  >
                    Choose From Gallery
                  </Text>
                </View>
                <View
                  style={{
                    width: "50%",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <TouchableOpacity>
                    <Image
                      source={require("../../LocallyTImages/Camera.png")}
                      style={{ height: 80, width: 80 }}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#283673",
                      marginTop: 0,
                      letterSpacing: 0,
                      fontFamily: "Helvetica-Bold",
                      fontSize: 13,
                    }}
                  >
                    Take a Photo
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>

      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 14,
        }}
      >
        <View
          style={{
            width: "42%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent: "flex-end",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "58%",
            alignItems: "flex-end",
            justifyContent: "flex-start",
            flexDirection: "row",
            height: 60,
          }}
        >
          <Image
            style={{ height: 30, width: 30, right: 2, bottom: 3 }}
            source={require("../../LocallyTImages/location.png")}
          />
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  lineHeight: 17,
                }}
              >
                Delivery to
              </Text>
              <Image
                source={require("../../LocallyTImages/DropDown.png")}
                style={{ height: 15, width: 15, left: 7, bottom: 2 }}
              />
            </View>
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                fontSize: 12,
                lineHeight: 17,
              }}
            >
              Rose Icon, Pimple Saudagar
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
          height: "78%",
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              width: width,
              alignItems: "center",
            }}
          >
            <View style={{ width: "90%", marginTop: 15 }}>
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: 17,
                  color: "#253f67",
                }}
              >
                Make a list of items to order
              </Text>

              <View
                style={{
                  height: 220,
                  width: "100%",
                  borderRadius: 20,
                  borderWidth: 0,
                  borderColor: "gray",
                  backgroundColor: "#FDEDEC",
                  elevation: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  paddingRight: 3,
                }}
              >
                <ScrollView
                  style={{ width: "100%", backgroundColor: "transparent", borderRadius:20 }}
                >
                  <View
                    style={{
                      width: "100%",
                      alignItems: "center",
                      justifyContent: "center",
                      paddingRight: 3,
                      borderRadius:20
                    }}
                  >
                    <View
                      style={{
                        width: "95%",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        height: 45,
                      }}
                    >
                      <TextInput
                        style={{
                          width: "55%",
                          height: 35,
                          justifyContent: "center",
                          borderBottomWidth: 1,
                          fontSize: 15,
                        }}
                        placeholder="Item 1"
                      />
                      <View
                        style={{
                          width: "24%",
                          height: 30,
                          alignItems: "center",
                          justifyContent: "space-around",
                          borderColor: "#253f67",
                          flexDirection: "row",
                          borderRadius: 30,
                          borderWidth: 1,
                        }}
                      >
                        <TouchableOpacity onPress={decrement}>
                          <Text
                            style={{
                              fontFamily: "Helvetica-Bold",
                              fontSize: 28,
                              lineHeight: 27,
                              color: "#fb5414",
                            }}
                          >
                            -
                          </Text>
                        </TouchableOpacity>
                        <Text
                          style={{
                            fontFamily: "Helvetica-Bold",
                            fontSize: 20,
                            color: "#fb5414",
                            lineHeight: 20,
                            top: 1.3,
                          }}
                        >
                          {counter}
                        </Text>
                        <TouchableOpacity onPress={increment}>
                          <Text
                            style={{
                              fontFamily: "Helvetica-Bold",
                              fontSize: 24,
                              lineHeight: 27,
                              color: "#fb5414",
                            }}
                          >
                            +
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          width: "17%",
                          height: 30,
                          alignItems: "center",
                          borderColor: "#253f67",
                          borderRadius: 30,
                          flexDirection: "row",
                        }}
                      >
                        <Picker
                          style={{
                            height: 36,
                            width: 200,
                            fontFamily: "Helvetica-Bold",
                            fontWeight: "bold",
                            paddingRight: 2,
                          }}
                          dropdownIconColor="#253f67"
                          selectedValue={selectedValue}
                          onValueChange={(itemValue, itemIndex) =>
                            setSelectedValue(itemValue)
                          }
                        >
                          <Picker.Item
                            fontFamily="Helvetica-Bold"
                            label="kg"
                            value="kg"
                          />
                          <Picker.Item label="Lit" value="Lit" />
                          <Picker.Item label="Nos" value="Nos" />
                        </Picker>
                        <Image
                          style={{
                            height: 14,
                            marginLeft: width - 322,
                            width: 14,
                            position: "absolute",
                            tintColor: "black",
                          }}
                          source={require("../../LocallyTImages/DropDown.png")}
                        ></Image>
                      </View>
                    </View>

                    <View
                      style={{
                        width: "95%",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        height: 45,
                      }}
                    >
                      <TextInput
                        style={{
                          width: "55%",
                          height: 35,
                          justifyContent: "center",
                          borderBottomWidth: 1,
                          fontSize: 15,
                        }}
                        placeholder="Item 2"
                      />
                      <View
                        style={{
                          width: "24%",
                          height: 30,
                          alignItems: "center",
                          justifyContent: "space-around",
                          borderColor: "#253f67",
                          borderWidth: 1,
                          flexDirection: "row",
                          borderRadius: 30,
                        }}
                      >
                        <TouchableOpacity onPress={decrement2}>
                          <Text
                            style={{
                              fontFamily: "Helvetica-Bold",
                              fontSize: 28,
                              lineHeight: 27,
                              color: "#fb5414",
                            }}
                          >
                            -
                          </Text>
                        </TouchableOpacity>
                        <Text
                          style={{
                            fontFamily: "Helvetica-Bold",
                            fontSize: 20,
                            lineHeight: 20,
                            top: 1.3,
                            color: "#fb5414",
                          }}
                        >
                          {counter2}
                        </Text>
                        <TouchableOpacity onPress={increment2}>
                          <Text
                            style={{
                              fontFamily: "Helvetica-Bold",
                              fontSize: 24,
                              lineHeight: 27,
                              color: "#fb5414",
                            }}
                          >
                            +
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          width: "17%",
                          height: 30,
                          alignItems: "center",
                          borderColor: "#253f67",
                          borderRadius: 30,
                          flexDirection: "row",
                        }}
                      >
                        <Picker
                          style={{
                            height: 36,
                            width: 200,
                            fontFamily: "Helvetica-Bold",
                            textAlign: "center",
                            fontWeight: "bold",
                          }}
                          dropdownIconColor="#253f67"
                          selectedValue={selectedValue2}
                          onValueChange={(itemValue, itemIndex) =>
                            setSelectedValue2(itemValue)
                          }
                        >
                          <Picker.Item label="Lit" value="Lit" />
                          <Picker.Item
                            fontFamily="Helvetica-Bold"
                            label="kg"
                            value="kg"
                          />
                          <Picker.Item label="Nos" value="Nos" />
                        </Picker>
                        <Image
                          style={{
                            height: 14,
                            marginLeft: width - 322,
                            width: 14,
                            position: "absolute",
                            tintColor: "black",
                          }}
                          source={require("../../LocallyTImages/DropDown.png")}
                        ></Image>
                      </View>
                    </View>
                    <View
                      style={{
                        width: "95%",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        height: 45,
                      }}
                    >
                      <TextInput
                        style={{
                          width: "55%",
                          height: 35,
                          justifyContent: "center",
                          borderBottomWidth: 1,
                          fontSize: 15,
                        }}
                        placeholder="Item 3"
                      />
                      <View
                        style={{
                          width: "24%",
                          height: 30,
                          alignItems: "center",
                          justifyContent: "space-around",
                          borderColor: "#253f67",
                          flexDirection: "row",
                          borderRadius: 30,
                          borderWidth: 1,
                        }}
                      >
                        <TouchableOpacity onPress={decrement3}>
                          <Text
                            style={{
                              fontFamily: "Helvetica-Bold",
                              fontSize: 28,
                              lineHeight: 27,
                              color: "#fb5414",
                            }}
                          >
                            -
                          </Text>
                        </TouchableOpacity>
                        <Text
                          style={{
                            fontFamily: "Helvetica-Bold",
                            fontSize: 20,
                            lineHeight: 20,
                            top: 1.3,
                            color: "#fb5414",
                          }}
                        >
                          {counter3}
                        </Text>
                        <TouchableOpacity onPress={increment3}>
                          <Text
                            style={{
                              fontFamily: "Helvetica-Bold",
                              fontSize: 24,
                              lineHeight: 27,
                              color: "#fb5414",
                            }}
                          >
                            +
                          </Text>
                        </TouchableOpacity>
                      </View>

                      <View
                        style={{
                          width: "17%",
                          height: 30,
                          alignItems: "center",
                          borderColor: "#253f67",
                          borderRadius: 30,
                          flexDirection: "row",
                        }}
                      >
                        <Picker
                          style={{
                            height: 36,
                            width: 200,
                            fontFamily: "Helvetica-Bold",
                            textAlign: "center",
                            fontWeight: "bold",
                          }}
                          dropdownIconColor="#253f67"
                          selectedValue={selectedValue3}
                          onValueChange={(itemValue, itemIndex) =>
                            setSelectedValue3(itemValue)
                          }
                        >
                          <Picker.Item label="Nos" value="Nos" />
                          <Picker.Item
                            fontFamily="Helvetica-Bold"
                            label="kg"
                            value="kg"
                          />
                          <Picker.Item label="Lit" value="Lit" />
                        </Picker>
                        <Image
                          style={{
                            height: 14,
                            marginLeft: width - 322,
                            width: 14,
                            position: "absolute",
                            tintColor: "black",
                          }}
                          source={require("../../LocallyTImages/DropDown.png")}
                        ></Image>
                      </View>
                    </View>
                    <View style={{ width: "100%" }}>
                      <Collapse>
                        <CollapseHeader
                          style={{
                            width: "100%",
                            height: 40,
                          }}
                        >
                          <View
                            style={{
                              width: "100%",
                              height: 30,
                              flexDirection: "row",
                              alignItems: "center",
                              marginTop: 10,
                              justifyContent: "flex-start",
                              paddingLeft: 10,
                            }}
                          >
                            <TouchableOpacity
                              style={{
                                height: 30,
                                width: 30,
                                backgroundColor: "#fb5414",
                                alignItems: "center",
                                borderRadius: 30,
                              }}
                            >
                              <Text
                                style={{
                                  color: "white",
                                  fontWeight: "bold",
                                  fontSize: 24,
                                  bottom: 2.5,
                                }}
                              >
                                +
                              </Text>
                            </TouchableOpacity>
                            <Text
                              style={{
                                fontFamily: "Helvetica",
                                fontSize: 16,
                                left: 10,
                              }}
                            >
                              Add More Items
                            </Text>
                          </View>
                        </CollapseHeader>
                        <CollapseBody
                          style={{
                            width: "100%",
                            backgroundColor: "#FDEDEC",
                            marginTop: 11,
                            paddingLeft: 10,
                            borderRadius: 20,
                          }}
                        >
                          <View
                            style={{
                              width: "100%",
                              alignItems: "center",
                              justifyContent: "center",
                              paddingRight: 3,
                            }}
                          >
                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 4"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  flexDirection: "row",
                                  borderRadius: 30,
                                  borderWidth: 1,
                                }}
                              >
                                <TouchableOpacity onPress={decrement4}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    color: "#fb5414",
                                    lineHeight: 20,
                                    top: 1.3,
                                  }}
                                >
                                  {counter4}
                                </Text>
                                <TouchableOpacity onPress={increment4}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>
                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    fontWeight: "bold",
                                    paddingRight: 2,
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue4}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue4(itemValue)
                                  }
                                >
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Lit" value="Lit" />
                                  <Picker.Item label="Nos" value="Nos" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>

                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 5"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  borderWidth: 1,
                                  flexDirection: "row",
                                  borderRadius: 30,
                                }}
                              >
                                <TouchableOpacity onPress={decrement5}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    lineHeight: 20,
                                    top: 1.3,
                                    color: "#fb5414",
                                  }}
                                >
                                  {counter5}
                                </Text>
                                <TouchableOpacity onPress={increment5}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>
                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    textAlign: "center",
                                    fontWeight: "bold",
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue5}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue5(itemValue)
                                  }
                                >
                                  <Picker.Item label="Lit" value="Lit" />
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Nos" value="Nos" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>
                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 6"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  flexDirection: "row",
                                  borderRadius: 30,
                                  borderWidth: 1,
                                }}
                              >
                                <TouchableOpacity onPress={decrement6}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    lineHeight: 20,
                                    top: 1.3,
                                    color: "#fb5414",
                                  }}
                                >
                                  {counter6}
                                </Text>
                                <TouchableOpacity onPress={increment6}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>

                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    textAlign: "center",
                                    fontWeight: "bold",
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue6}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue6(itemValue)
                                  }
                                >
                                  <Picker.Item label="Nos" value="Nos" />
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Lit" value="Lit" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>
                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 7"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  flexDirection: "row",
                                  borderRadius: 30,
                                  borderWidth: 1,
                                }}
                              >
                                <TouchableOpacity onPress={decrement7}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    lineHeight: 20,
                                    top: 1.3,
                                    color: "#fb5414",
                                  }}
                                >
                                  {counter7}
                                </Text>
                                <TouchableOpacity onPress={increment7}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>

                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    textAlign: "center",
                                    fontWeight: "bold",
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue7}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue7(itemValue)
                                  }
                                >
                                  <Picker.Item label="Nos" value="Nos" />
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Lit" value="Lit" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>
                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 8"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  flexDirection: "row",
                                  borderRadius: 30,
                                  borderWidth: 1,
                                }}
                              >
                                <TouchableOpacity onPress={decrement8}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    lineHeight: 20,
                                    top: 1.3,
                                    color: "#fb5414",
                                  }}
                                >
                                  {counter8}
                                </Text>
                                <TouchableOpacity onPress={increment8}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>

                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    textAlign: "center",
                                    fontWeight: "bold",
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue8}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue8(itemValue)
                                  }
                                >
                                  <Picker.Item label="Nos" value="Nos" />
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Lit" value="Lit" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>
                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 9"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  flexDirection: "row",
                                  borderRadius: 30,
                                  borderWidth: 1,
                                }}
                              >
                                <TouchableOpacity onPress={decrement9}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    lineHeight: 20,
                                    top: 1.3,
                                    color: "#fb5414",
                                  }}
                                >
                                  {counter9}
                                </Text>
                                <TouchableOpacity onPress={increment9}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>

                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    textAlign: "center",
                                    fontWeight: "bold",
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue9}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue9(itemValue)
                                  }
                                >
                                  <Picker.Item label="Nos" value="Nos" />
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Lit" value="Lit" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>
                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 10"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  flexDirection: "row",
                                  borderRadius: 30,
                                  borderWidth: 1,
                                }}
                              >
                                <TouchableOpacity onPress={decrement10}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    lineHeight: 20,
                                    top: 1.3,
                                    color: "#fb5414",
                                  }}
                                >
                                  {counter10}
                                </Text>
                                <TouchableOpacity onPress={increment10}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>

                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    textAlign: "center",
                                    fontWeight: "bold",
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue10}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue10(itemValue)
                                  }
                                >
                                  <Picker.Item label="Nos" value="Nos" />
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Lit" value="Lit" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>
                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 11"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  flexDirection: "row",
                                  borderRadius: 30,
                                  borderWidth: 1,
                                }}
                              >
                                <TouchableOpacity onPress={decrement11}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    lineHeight: 20,
                                    top: 1.3,
                                    color: "#fb5414",
                                  }}
                                >
                                  {counter11}
                                </Text>
                                <TouchableOpacity onPress={increment11}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>

                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    textAlign: "center",
                                    fontWeight: "bold",
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue11}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue11(itemValue)
                                  }
                                >
                                  <Picker.Item label="Nos" value="Nos" />
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Lit" value="Lit" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>
                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 12"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  flexDirection: "row",
                                  borderRadius: 30,
                                  borderWidth: 1,
                                }}
                              >
                                <TouchableOpacity onPress={decrement12}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    lineHeight: 20,
                                    top: 1.3,
                                    color: "#fb5414",
                                  }}
                                >
                                  {counter12}
                                </Text>
                                <TouchableOpacity onPress={increment12}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>

                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    textAlign: "center",
                                    fontWeight: "bold",
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue12}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue12(itemValue)
                                  }
                                >
                                  <Picker.Item label="Nos" value="Nos" />
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Lit" value="Lit" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>
                            <View
                              style={{
                                width: "95%",
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "space-between",
                                height: 45,
                              }}
                            >
                              <TextInput
                                style={{
                                  width: "55%",
                                  height: 35,
                                  justifyContent: "center",
                                  borderBottomWidth: 1,
                                  fontSize: 15,
                                }}
                                placeholder="Item 13"
                              />
                              <View
                                style={{
                                  width: "24%",
                                  height: 30,
                                  alignItems: "center",
                                  justifyContent: "space-around",
                                  borderColor: "#253f67",
                                  flexDirection: "row",
                                  borderRadius: 30,
                                  borderWidth: 1,
                                }}
                              >
                                <TouchableOpacity onPress={decrement13}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 28,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    -
                                  </Text>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontFamily: "Helvetica-Bold",
                                    fontSize: 20,
                                    lineHeight: 20,
                                    top: 1.3,
                                    color: "#fb5414",
                                  }}
                                >
                                  {counter13}
                                </Text>
                                <TouchableOpacity onPress={increment13}>
                                  <Text
                                    style={{
                                      fontFamily: "Helvetica-Bold",
                                      fontSize: 24,
                                      lineHeight: 27,
                                      color: "#fb5414",
                                    }}
                                  >
                                    +
                                  </Text>
                                </TouchableOpacity>
                              </View>

                              <View
                                style={{
                                  width: "17%",
                                  height: 30,
                                  alignItems: "center",
                                  borderColor: "#253f67",
                                  borderRadius: 30,
                                  flexDirection: "row",
                                }}
                              >
                                <Picker
                                  style={{
                                    height: 36,
                                    width: 200,
                                    fontFamily: "Helvetica-Bold",
                                    textAlign: "center",
                                    fontWeight: "bold",
                                  }}
                                  dropdownIconColor="#253f67"
                                  selectedValue={selectedValue13}
                                  onValueChange={(itemValue, itemIndex) =>
                                    setSelectedValue13(itemValue)
                                  }
                                >
                                  <Picker.Item label="Nos" value="Nos" />
                                  <Picker.Item
                                    fontFamily="Helvetica-Bold"
                                    label="kg"
                                    value="kg"
                                  />
                                  <Picker.Item label="Lit" value="Lit" />
                                </Picker>
                                <Image
                                  style={{
                                    height: 14,
                                    marginLeft: width - 322,
                                    width: 14,
                                    position: "absolute",
                                    tintColor: "black",
                                  }}
                                  source={require("../../LocallyTImages/DropDown.png")}
                                ></Image>
                              </View>
                            </View>
                            <View style={{height:8, width:'100%'}}></View>
                          </View>
                        </CollapseBody>
                      </Collapse>
                    </View>
                    {/*  */}
                  </View>
                </ScrollView>
              </View>

              <View
                style={{
                  width: "100%",
                  height: 43,
                  justifyContent: "flex-end",
                  alignItems: "flex-end",
                  marginTop: 2,
                }}
              >
                <TouchableOpacity
                  style={{
                    height: 33,
                    width: "45%",
                    backgroundColor: "#B2381D",
                    borderRadius: 20,
                    justifyContent: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      textAlign: "center",
                      fontSize: 14,
                      fontFamily: "Helvetica-Bold",
                    }}
                  >
                    Copy previous list
                  </Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  width: "100%",
                  height: 29,
                  marginTop: 10,
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "row",
                }}
              >
                <View
                  style={{
                    height: 1,
                    width: "45%",
                    borderWidth: 1,
                    borderColor: "#fb4514",
                  }}
                ></View>
                <View
                  style={{
                    height: 29,
                    width: "10%",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 15 }}>
                    OR
                  </Text>
                </View>
                <View
                  style={{
                    height: 1,
                    width: "45%",
                    borderWidth: 1,
                    borderColor: "#fb4514",
                  }}
                ></View>
              </View>

              <View
                style={{
                  height: 130,
                  width: "100%",
                  backgroundColor: "white",
                  borderStyle: "dashed",
                  marginTop: 0,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <TouchableOpacity onPress={() => setModalOpen(true)}>
                  <Image
                    source={require("../../LocallyTImages/Uploadcamera.png")}
                    style={{
                      height: 90,
                      width: 110,
                      marginTop: 0,
                      tintColor: "#fb5414",
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              height: 2,
              width: width,
              backgroundColor: "white",
              marginTop: 5,
            }}
          ></View>
        </ScrollView>
      </View>
      <View
        style={{ height: 10, width: width, backgroundColor: "white" }}
      ></View>

      <View
        style={{
          width: "100%",
          alignItems: "center",
          backgroundColor: "white",
        }}
      >
        <TouchableOpacity
          onPress={() => navigation.navigate("Store")}
          style={{
            height: 40,
            width: width - 190,
            backgroundColor: "#fb5414",
            borderRadius: 39,
            justifyContent: "center",
            bottom: 8,
          }}
        >
          <Text
            style={{
              color: "white",
              fontFamily: "Helvetica-Bold",
              textAlign: "center",
              fontSize: 17,
            }}
          >
            Proceed
          </Text>
        </TouchableOpacity>
      </View>
      <View
        style={{ height: 10, width: width, backgroundColor: "white" }}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({});
