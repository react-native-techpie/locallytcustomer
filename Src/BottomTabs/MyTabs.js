import React from "react";
import { View, Text, StyleSheet, Dimensions, Image } from "react-native";

const { height, width } = Dimensions.get("window");



const ShareStack = createStackNavigator();

function ShareStackScreens() {
  return (
    <ShareStack.Navigator screenOptions={{ headerShown: null }}>
      <ShareStack.Screen name="ShareList" component={ShareList} />
      <ShareStack.Screen name="Store" component={Store} />
      <ShareStack.Screen name="ShareStore" component={ShareStore} />
      <ShareStack.Screen name="ChooseStore" component={ChooseStore} />
      {/* <OrderStack.Screen name="MyOrders" component={MyOrders} /> */}
    </ShareStack.Navigator>
  );
}



const OrderStack = createStackNavigator();

function OrderStackScreens() {
  return (
    <OrderStack.Navigator screenOptions={{ headerShown: null }}>
      <OrderStack.Screen name="MyOrders" component={MyOrders} />
      <OrderStack.Screen name="OrderDetails" component={OrderDetails} />
      <OrderStack.Screen name="StoreInfo" component={StoreInfo} />
    </OrderStack.Navigator>
  );
}





import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import ShareList from "./ShareList";
import MyOrders from "./MyOrders";
import DealsNearBy from "./DealsNearBy";
import { createStackNavigator } from "@react-navigation/stack";
import Store from "./NestedScreens/Store";
import ShareStore from "./NestedScreens/ShareStore";
import OrderDetails from "./NestedScreens/OrderDetails";
import StoreInfo from "./NestedScreens/StoreInfo";
import ChooseStore from "./NestedScreens/ChooseStore";

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {
          backgroundColor: "#FFFF",
          height: 58,
          elevation: 20,
          bottom: 0,
        },
      }}
    >
      <Tab.Screen
        name="ShareStackScreens"
        component={ShareStackScreens}
        options={{
          tabBarIcon: ({ focused }) => (
            <View
              style={{ justifyContent: "center", alignItem: "center", left: 4 }}
            >
              <Image
                source={require("../../LocallyTImages/ShareList.png")}
                resizeMode="contain"
                style={{
                  width: 30,
                  height: 30,
                  alignContent: "center",
                  tintColor: focused ? "#fb5414" : "black",
                  alignSelf: "center",
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: "Helvetica-Bold",
                  color: focused ? "#fb5414" : "black",
                }}
              >
                Share List
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="OrderStackScreens"
        component={OrderStackScreens}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={{ justifyContent: "center", alignItem: "center" }}>
              <Image
                source={require("../../LocallyTImages/OrderFlow.png")}
                resizeMode="contain"
                style={{
                  width: 30,
                  height: 30,
                  alignContent: "center",
                  tintColor: focused ? "#fb5414" : "black",
                  alignSelf: "center",
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: "Helvetica-Bold",
                  color: focused ? "#fb5414" : "black",
                }}
              >
                Order Flow
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="DealsNearBy"
        component={DealsNearBy}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={{ justifyContent: "center", alignItem: "center" }}>
              <Image
                source={require("../../LocallyTImages/Deals.png")}
                resizeMode="contain"
                style={{
                  width: 30,
                  height: 30,
                  alignContent: "center",
                  tintColor: focused ? "#fb5414" : "black",
                  alignSelf: "center",
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  fontFamily: "Helvetica-Bold",
                  color: focused ? "#fb5414" : "black",
                }}
              >
                Deals Nearby
              </Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default MyTabs;
