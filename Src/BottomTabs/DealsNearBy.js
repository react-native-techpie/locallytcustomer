import React, {useState} from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { Picker } from "@react-native-picker/picker";

const { height, width } = Dimensions.get("window");

export default function DealsNearBy({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 90,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 20,
        }}
      >
        <View
          style={{
            width: "40%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 50,
            justifyContent: "center",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "60%",
            paddingLeft: 20,
            alignItems: "center",
            paddingRight: 20,
            justifyContent: "center",
            flexDirection: "row",
            bottom: 2,
          }}
        >
          <Image
            style={{ height: 30, width: 30, right: 2 }}
            source={require("../../LocallyTImages/location.png")}
          />
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  lineHeight: 17,
                }}
              >
                Delivery to
              </Text>
              <Image
                source={require("../../LocallyTImages/DropDown.png")}
                style={{ height: 15, width: 15, left: 7, bottom: 2 }}
              />
            </View>
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                fontSize: 12,
                lineHeight: 17,
              }}
            >
              Rose Icon, Pimple Saudagar
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
          height: "80%",
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              width: width,
              alignItems: "center",
              borderTopEndRadius: 25,
              borderTopStartRadius: 25,
              marginTop: 30,
            }}
          ></View>
          <View
            style={{
              height: 28,
              width: width,
              backgroundColor: "white",
              marginTop: 5,
            }}
          ></View>
        </ScrollView>
      </View>
      <View
        style={{ height: 20, width: width, backgroundColor: "white" }}
      ></View>

      <View
        style={{ height: 20, width: width, backgroundColor: "white" }}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({});
