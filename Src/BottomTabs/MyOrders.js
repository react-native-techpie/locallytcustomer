import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import { Picker } from "@react-native-picker/picker";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { NavigationContainer } from "@react-navigation/native";
import Confirm from "./Orders/Confirm";
import Ongoing from "./Orders/Ongoing";
import Completed from "./Orders/Completed";

const Tab = createMaterialTopTabNavigator();

function HeaderTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: "white",
          height: 70,
          width: "90%",
          left: 20,
          right: 20,
          top: 10,
          borderRadius: 24,
          elevation: 1,
          borderWidth: 1.3,
          borderColor: "#cfe2f3",
          justifyContent: "center",
        },
      }}
    >
      <Tab.Screen
        options={{
          title: ({ focused }) => (
            <View
              style={{
                height: 50,
                borderRadius: 20,
                width: 100,
                alignItems: "center",
                justifyContent: "center",
                top: 0,
                backgroundColor: focused ? "#fb5414" : "#FFFF",
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 16,
                  color: focused ? "#FFFF" : "#253f67",
                }}
              >
                Confirm order
              </Text>
            </View>
          ),
        }}
        name="Confirm"
        component={Confirm}
      />
      <Tab.Screen
        options={{
          title: ({ focused }) => (
            <View
              style={{
                height: 50,
                borderRadius: 20,
                width: 100,
                alignItems: "center",
                justifyContent: "center",
                top: 0,
                backgroundColor: focused ? "#fb5414" : "#FFFF",
              }}
            >
              <Text
                style={{
                  color: focused ? "#FFFF" : "#253f67",
                  textAlign: "center",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 16,
                }}
              >
                Ongoing orders
              </Text>
            </View>
          ),
        }}
        name="Ongoing"
        component={Ongoing}
      />
      <Tab.Screen
        options={{
          title: ({ focused }) => (
            <View
              style={{
                height: 50,
                borderRadius: 20,
                width: 100,
                alignItems: "center",
                justifyContent: "center",
                top: 0,
                backgroundColor: focused ? "#fb5414" : "#FFFF",
              }}
            >
              <Text
                style={{
                  color: focused ? "#FFFF" : "#253f67",
                  textAlign: "center",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 16,
                }}
              >
                Completed orders
              </Text>
            </View>
          ),
        }}
        name="Completed"
        component={Completed}
      />
    </Tab.Navigator>
  );
}

const { height, width } = Dimensions.get("window");

export default function MyOrders({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 14,
        }}
      >
        <View
          style={{
            width: "42%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent: "flex-end",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "58%",
            alignItems: "flex-end",
            justifyContent: "flex-start",
            flexDirection: "row",
            height: 60,
          }}
        >
          <Image
            style={{ height: 30, width: 30, right: 2, bottom: 3 }}
            source={require("../../LocallyTImages/location.png")}
          />
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  lineHeight: 17,
                }}
              >
                Delivery to
              </Text>
              <Image
                source={require("../../LocallyTImages/DropDown.png")}
                style={{ height: 15, width: 15, left: 7, bottom: 2 }}
              />
            </View>
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                fontSize: 12,
                lineHeight: 17,
              }}
            >
              Rose Icon, Pimple Saudagar
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          height: "90%",
          width: "100%",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
        }}
      >
        <HeaderTabs />
      </View>
      <View
        style={{ height:0, width: "100%", backgroundColor: "white" }}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({});
