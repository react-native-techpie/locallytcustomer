import React, {useState} from "react";
import { View, Text, ScrollView, Image, TouchableOpacity } from "react-native";
import Slider from "@react-native-community/slider";

export default function Ongoing({ navigation }) {

const [range, setRange] = useState("50%");
const [sliding, setSliding] = useState("Inactive");

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{
        backgroundColor: "white",

        height: "100%",
        width: "100%",
      }}
    >
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          marginTop: 15,
          height: "100%",
          paddingBottom: 100,
        }}
      >
        <View
          style={{
            height: 245,
            width: "90%",
            backgroundColor: "white",
            borderWidth: 1.5,
            borderColor: "#cfe2f3",
            borderRadius: 20,
            elevation: 1,
            alignItems: "center",
            marginTop: 0,
          }}
        >
          <View
            style={{
              height: "33%",
              width: "100%",
              //   backgroundColor: "red",
              borderTopStartRadius: 20,
              borderTopEndRadius: 20,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <View style={{ height: "100%", width: "28%" }}>
              <Image
                source={require("../../../LocallyTImages/Shop.png")}
                style={{ height: 60, width: 80, left: 7, top: 11 }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                height: "100%",
                width: "76%",
                alignItems: "flex-end",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "60%",
                  alignItems: "center",
                  paddingTop: 10,
                  // justifyContent: "center",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 13.2,
                      lineHeight: 22,
                      fontFamily: "Helvetica-Bold",
                    }}
                  >
                    Venus super market
                  </Text>
                  <Image
                    source={require("../../../LocallyTImages/heart.jpg")}
                    style={{ height: 20, width: 20, left: 2 }}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    width: "95%",
                    justifyContent: "space-between",
                  }}
                >
                  <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                    Thergaon | 2.5 kms
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                      left: 9,
                    }}
                  >
                    <Image
                      source={require("../../../LocallyTImages/star.png")}
                      style={{ height: 15, width: 15, right: 6 }}
                    />
                    <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                      3.9
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  height: "100%",
                  width: "38%",
                  alignItems: "center",
                }}
              >
                <TouchableOpacity
                  style={{
                    height: 38,
                    backgroundColor: "#253f67",
                    width: 38,
                    borderRadius: 70,
                    justifyContent: "center",
                    marginTop: 10,
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Wcontact.png")}
                    style={{
                      height: 30,
                      width: 30,
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              height: "16%",
              width: "100%",
              alignItems: "center",
              // backgroundColor: "#B1C1DA",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View
              style={{
                height: "100%",
                width: "31%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica" }}>Order placed</Text>
              <Text style={{ fontFamily: "Helvetica-Bold", lineHeight: 20 }}>
                10:43 AM
              </Text>
            </View>
            <View
              style={{
                height: "100%",
                width: "31%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica" }}>Delivery</Text>
              <Text style={{ fontFamily: "Helvetica-Bold", lineHeight: 20 }}>
                11:43 AM
              </Text>
            </View>
            <View
              style={{
                height: "100%",
                width: "31%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica" }}>Total payble</Text>
              <Text style={{ fontFamily: "Helvetica-Bold", lineHeight: 20 }}>
                Rs. xxx.xx
              </Text>
            </View>
          </View>
          <View
            style={{
              height: 33,
              width: "77%",
              alignItems: "center",
              justifyContent: "center",
              bottom: 4,
            }}
          >
            <View
              style={{
                width: "100%",
                height: 1,
                borderWidth: 1.5,
                borderColor: "pink",
                marginTop: 12,
              }}
            ></View>
            <View
              style={{
                height: "100%",
                width: "100%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                position: "absolute",
                top: 6,
              }}
            >
              {/* <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "#fb5414",
                  borderRadius: 10,
                  position: "absolute",
                }}
              ></TouchableOpacity> */}
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "#fb5414",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "gray",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "gray",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              height: "17%",
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View
              style={{
                width: "30%",
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica",
                  color: "#253f67",
                  textAlign: "center",
                }}
              >
                At
              </Text>
              <Text
                style={{
                  fontFamily: "Helvetica",
                  color: "#253f67",
                  textAlign: "center",
                }}
              >
                Store
              </Text>
            </View>
            <View
              style={{
                width: "30%",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Out for delivery
              </Text>
            </View>
            <View
              style={{
                width: "30%",
                alignItems: "center",
                // justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Delivered
              </Text>
            </View>
          </View>

          <View
            style={{
              height: "15%",
              width: "100%",
              backgroundColor: "white",
              alignItems: "center",
              justifyContent: "flex-start",
              borderBottomStartRadius: 21,
              borderBottomEndRadius: 21,
            }}
          >
            <TouchableOpacity
              onPress={() => navigation.navigate("OrderDetails")}
              style={{
                height: 30,
                backgroundColor: "#fb5414",
                width: "50%",
                borderRadius: 70,
                justifyContent: "center",
                marginTop: 11,
              }}
            >
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  textAlign: "center",
                  fontSize: 16,
                }}
              >
                View Details
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            height: 245,
            width: "90%",
            backgroundColor: "white",
            borderWidth: 1.5,
            borderColor: "#cfe2f3",
            borderRadius: 20,
            elevation: 1,
            alignItems: "center",
            marginTop: 15,
          }}
        >
          <View
            style={{
              height: "33%",
              width: "100%",
              //   backgroundColor: "red",
              borderTopStartRadius: 20,
              borderTopEndRadius: 20,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <View style={{ height: "100%", width: "28%" }}>
              <Image
                source={require("../../../LocallyTImages/Shop.png")}
                style={{ height: 60, width: 80, left: 7, top: 11 }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                height: "100%",
                width: "76%",
                alignItems: "flex-end",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "60%",
                  alignItems: "center",
                  paddingTop: 10,
                  // justifyContent: "center",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 13.2,
                      lineHeight: 22,
                      fontFamily: "Helvetica-Bold",
                    }}
                  >
                    Venus super market
                  </Text>
                  <Image
                    source={require("../../../LocallyTImages/heart.jpg")}
                    style={{ height: 20, width: 20, left: 2 }}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    width: "95%",
                    justifyContent: "space-between",
                  }}
                >
                  <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                    Thergaon | 2.5 kms
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                      left: 9,
                    }}
                  >
                    <Image
                      source={require("../../../LocallyTImages/star.png")}
                      style={{ height: 15, width: 15, right: 6 }}
                    />
                    <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                      3.9
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  height: "100%",
                  width: "38%",
                  alignItems: "center",
                }}
              >
                <TouchableOpacity
                  style={{
                    height: 38,
                    backgroundColor: "#253f67",
                    width: 38,
                    borderRadius: 70,
                    justifyContent: "center",
                    marginTop: 10,
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Wcontact.png")}
                    style={{
                      height: 30,
                      width: 30,
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              height: "16%",
              width: "100%",
              alignItems: "center",
              // backgroundColor: "#B1C1DA",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View
              style={{
                height: "100%",
                width: "31%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica" }}>Order placed</Text>
              <Text style={{ fontFamily: "Helvetica-Bold", lineHeight: 20 }}>
                10:43 AM
              </Text>
            </View>
            <View
              style={{
                height: "100%",
                width: "31%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica" }}>Delivery</Text>
              <Text style={{ fontFamily: "Helvetica-Bold", lineHeight: 20 }}>
                11:43 AM
              </Text>
            </View>
            <View
              style={{
                height: "100%",
                width: "31%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica" }}>Total payble</Text>
              <Text style={{ fontFamily: "Helvetica-Bold", lineHeight: 20 }}>
                Rs. xxx.xx
              </Text>
            </View>
          </View>
          <View
            style={{
              height: 33,
              width: "77%",
              alignItems: "center",
              justifyContent: "center",
              bottom: 4,
            }}
          >
            <View
              style={{
                width: "100%",
                height: 1,
                borderWidth: 1.5,
                borderColor: "pink",
                marginTop: 12,
              }}
            ></View>
            <View
              style={{
                height: "100%",
                width: "100%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                position: "absolute",
                top: 6,
              }}
            >
              {/* <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "#fb5414",
                  borderRadius: 10,
                  position: "absolute",
                }}
              ></TouchableOpacity> */}
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "#fb5414",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "gray",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "gray",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              height: "17%",
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View
              style={{
                width: "30%",
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica",
                  color: "#253f67",
                  textAlign: "center",
                }}
              >
                At
              </Text>
              <Text
                style={{
                  fontFamily: "Helvetica",
                  color: "#253f67",
                  textAlign: "center",
                }}
              >
                Store
              </Text>
            </View>
            <View
              style={{
                width: "30%",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Out for delivery
              </Text>
            </View>
            <View
              style={{
                width: "30%",
                alignItems: "center",
                // justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Delivered
              </Text>
            </View>
          </View>

          <View
            style={{
              height: "15%",
              width: "100%",
              backgroundColor: "white",
              alignItems: "center",
              justifyContent: "flex-start",
              borderBottomStartRadius: 21,
              borderBottomEndRadius: 21,
            }}
          >
            <TouchableOpacity
              onPress={() => navigation.navigate("OrderDetails")}
              style={{
                height: 30,
                backgroundColor: "#fb5414",
                width: "50%",
                borderRadius: 70,
                justifyContent: "center",
                marginTop: 11,
              }}
            >
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  textAlign: "center",
                  fontSize: 16,
                }}
              >
                View Details
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        {/* <View
          style={{
            height: 300,
            width: "90%",
            backgroundColor: "white",
            borderWidth: 1.5,
            borderColor: "#253f67",
            borderRadius: 20,
            elevation: 4,
            alignItems: "center",
            marginTop: 20,
          }}
        >
          <View
            style={{
              height: "38%",
              width: "100%",
              //   backgroundColor: "red",
              borderTopStartRadius: 20,
              borderTopEndRadius: 20,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <View style={{ height: "100%", width: "30%" }}>
              <Image
                source={require("../../../LocallyTImages/Shop.png")}
                style={{ height: 60, width: 80, left: 7, top: 11 }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                height: "100%",
                width: "70%",
                alignItems: "flex-end",
              }}
            >
              <View style={{ height: "100%", width: "60%" }}>
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                    top: 11,
                    lineHeight: 24,
                  }}
                >
                  Name_1
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                    top: 11,
                    lineHeight: 24,
                  }}
                >
                  Rating *4.1
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                    top: 11,
                    lineHeight: 24,
                  }}
                >
                  (100+)
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    color: "#253f67",
                    lineHeight: 39,
                    fontSize: 17.4,
                  }}
                >
                  UPI/Wallet, COD
                </Text>
              </View>

              <View
                style={{
                  height: "100%",
                  width: "40%",
                  alignItems: "center",
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                    top: 11,
                    lineHeight: 24,
                  }}
                >
                  Contact
                </Text>
                <TouchableOpacity
                  style={{
                    height: 40,
                    backgroundColor: "#253f67",
                    width: 40,
                    borderRadius: 70,
                    justifyContent: "center",
                    marginTop: 16,
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Wcontact.png")}
                    style={{
                      height: 32,
                      width: 32,
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              height: "18%",
              width: "100%",
              alignItems: "center",
              backgroundColor: "#B1C1DA",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View
              style={{
                height: "100%",
                width: "31%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica" }}>Order placed</Text>
              <Text style={{ fontFamily: "Helvetica-Bold", lineHeight: 20 }}>
                10:43 AM
              </Text>
            </View>
            <View
              style={{
                height: "100%",
                width: "31%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica" }}>Delivered by</Text>
              <Text style={{ fontFamily: "Helvetica-Bold", lineHeight: 20 }}>
                11:43 AM
              </Text>
            </View>
            <View
              style={{
                height: "100%",
                width: "31%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica" }}>Total payble</Text>
              <Text style={{ fontFamily: "Helvetica-Bold", lineHeight: 20 }}>
                Rs. xxx.xx
              </Text>
            </View>
          </View>
          <View
            style={{
              height: "27%",
              width: "90%",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View
              style={{
                width: "16%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica",
                  color: "#253f67",
                  textAlign: "center",
                }}
              >
                At Store
              </Text>
            </View>
            <View
              style={{
                width: "30%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Out for delivery
              </Text>
            </View>
            <View
              style={{
                width: "29%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Delivered
              </Text>
            </View>
          </View>

          <View
            style={{
              height: "17%",
              width: "100%",
              backgroundColor: "white",
              alignItems: "center",
              justifyContent: "flex-start",
              borderBottomStartRadius: 21,
              borderBottomEndRadius: 21,
            }}
          >
            <TouchableOpacity
              onPress={() => navigation.navigate("OrderDetails")}
              style={{
                height: 37,
                backgroundColor: "#fb5414",
                width: 147,
                borderRadius: 70,
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  textAlign: "center",
                  fontSize: 16,
                }}
              >
                View Details
              </Text>
            </TouchableOpacity>
          </View>
        </View> */}
      </View>
    </ScrollView>
  );
}
