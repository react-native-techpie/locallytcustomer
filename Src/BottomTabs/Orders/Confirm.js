import React, { useState } from "react";
import { View, Text, ScrollView, Image, TouchableOpacity } from "react-native";
import Slider from "@react-native-community/slider";

export default function Confirm({ navigation }) {
  const [range, setRange] = useState("50%");
  const [sliding, setSliding] = useState("Inactive");

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{
        backgroundColor: "white",

        height: "100%",
        width: "100%",
      }}
    >
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          marginTop: 14,
          height: "100%",
          paddingBottom: 100,
        }}
      >
        <View
          style={{
            height: 245,
            width: "90%",
            backgroundColor: "white",
            borderWidth: 1.5,
            borderColor: "#cfe2f3",
            borderRadius: 20,
            elevation: 1,
            alignItems: "center",
          }}
        >
          <View
            style={{
              height: "50%",
              width: "100%",
              //   backgroundColor: "red",
              borderTopStartRadius: 20,
              borderTopEndRadius: 20,
              flexDirection: "row",
            }}
          >
            <View style={{ height: "100%", width: "30%" }}>
              <Image
                source={require("../../../LocallyTImages/Shop.png")}
                style={{ height: 60, width: 80, left: 7, top: 11 }}
              />
            </View>
            <View
              style={{
                width: "70%",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "95%",
                  justifyContent: "space-between",
                }}
              >
                <View>
                  <Text style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}>
                    Venus super market
                  </Text>
                  <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                    Thergaon | 2.5 kms
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    height: 33,
                    backgroundColor: "#253f67",
                    width: 33,
                    borderRadius: 70,
                    justifyContent: "center",
                    marginTop: 6,
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Wcontact.png")}
                    style={{
                      height: 27,
                      width: 27,
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  width: "95%",
                  justifyContent: "space-between",
                  borderBottomWidth: 1.3,
                  borderColor: "gray",
                }}
              >
                <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                  Delivery:1 Hrs | No min order value
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  width: "95%",
                  justifyContent: "space-between",
                  height: 50,
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    width: "100%",
                    // justifyContent: "center",
                    height: 50,
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Percent.png")}
                    style={{
                      height: 29,
                      width: 29,
                    }}
                  />
                  <View
                    style={{
                      height: "100%",
                      // alignItems: "center",
                      justifyContent: "center",
                      width: "80%",
                      left: 4,
                    }}
                  >
                    <Text style={{ fontSize: 11.5, fontFamily: "Helvetica" }}>
                      10% off on orders above Rs. 500
                    </Text>
                    <Text style={{ fontSize: 11, fontFamily: "Helvetica" }}>
                      15% off on orders above Rs. 1000
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              height: 25,
              width: "66%",
              alignItems: "center",
              justifyContent: "center",
              bottom: 4,
            }}
          >
            <View
              style={{
                width: "100%",
                height: 1,
                borderWidth: 1.5,
                borderColor: "pink",
                marginTop: 12,
              }}
            ></View>
            <View
              style={{
                height: "100%",
                width: "100%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                position: "absolute",
                top: 6,
              }}
            >
              {/* <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "#fb5414",
                  borderRadius: 10,
                  position: "absolute",
                }}
              ></TouchableOpacity> */}
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "#fb5414",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "gray",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "gray",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
            </View>
          </View>
          {/* <Slider
            style={{
              height: 10,
              width: "95%",
              // position: "absolute",
              marginTop: 5,
              borderWidth: 5,
              borderColor: "white",
            }}
            height={20}
            minimumValue={0}
            maximumValue={1}
            minimumTrackTintColor="#283673"
            maximumTrackTintColor="white"
            thumbTintColor="#283673"
            value={0.5}
            onValueChange={(value) => setRange(parseInt(value * 100) + "%")}
            onSlidingStart={() => setSliding("Sliding")}
            onSlidingComplete={() => setSliding("Inactive")}
          /> */}
          <View
            style={{
              height: "15%",
              width: "90%",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              // marginTop:12
            }}
          >
            <View
              style={{
                width: "30%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Share list with store
              </Text>
            </View>
            <View
              style={{
                width: "30%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Response from store
              </Text>
            </View>
            <View
              style={{
                width: "29%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica",
                  color: "#253f67",
                  textAlign: "center",
                }}
              >
                Confirm order
              </Text>
            </View>
          </View>
          <TouchableOpacity
            // onPress={() => navigation.navigate("OrderDetails")}
            style={{
              height: 33,
              backgroundColor: "#fb5414",
              width: "50%",
              borderRadius: 70,
              justifyContent: "center",
              marginTop: 14,
            }}
          >
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                textAlign: "center",
                fontSize: 15.6,
              }}
            >
              Cancel Request
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            height: 245,
            width: "90%",
            backgroundColor: "white",
            borderWidth: 1.5,
            borderColor: "#cfe2f3",
            borderRadius: 20,
            elevation: 1,
            alignItems: "center",
            marginTop: 10,
          }}
        >
          <View
            style={{
              height: "50%",
              width: "100%",
              //   backgroundColor: "red",
              borderTopStartRadius: 20,
              borderTopEndRadius: 20,
              flexDirection: "row",
            }}
          >
            <View style={{ height: "100%", width: "30%" }}>
              <Image
                source={require("../../../LocallyTImages/Shop.png")}
                style={{ height: 60, width: 80, left: 7, top: 11 }}
              />
            </View>
            <View
              style={{
                width: "70%",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "95%",
                  justifyContent: "space-between",
                }}
              >
                <View>
                  <Text style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}>
                    Venus super market
                  </Text>
                  <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                    Thergaon | 2.5 kms
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    height: 33,
                    backgroundColor: "#253f67",
                    width: 33,
                    borderRadius: 70,
                    justifyContent: "center",
                    marginTop: 6,
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Wcontact.png")}
                    style={{
                      height: 27,
                      width: 27,
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  width: "95%",
                  justifyContent: "space-between",
                  borderBottomWidth: 1.3,
                  borderColor: "gray",
                }}
              >
                <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                  Delivery:1 Hrs | No min order value
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  width: "95%",
                  justifyContent: "space-between",
                  height: 50,
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    width: "100%",
                    // justifyContent: "center",
                    height: 50,
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Percent.png")}
                    style={{
                      height: 29,
                      width: 29,
                    }}
                  />
                  <View
                    style={{
                      height: "100%",
                      // alignItems: "center",
                      justifyContent: "center",
                      width: "80%",
                      left: 4,
                    }}
                  >
                    <Text style={{ fontSize: 11.5, fontFamily: "Helvetica" }}>
                      10% off on orders above Rs. 500
                    </Text>
                    <Text style={{ fontSize: 11, fontFamily: "Helvetica" }}>
                      15% off on orders above Rs. 1000
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              height: 25,
              width: "66%",
              alignItems: "center",
              justifyContent: "center",
              bottom: 4,
            }}
          >
            <View
              style={{
                width: "100%",
                height: 1,
                borderWidth: 1.5,
                borderColor: "pink",
                marginTop: 12,
              }}
            ></View>
            <View
              style={{
                height: "100%",
                width: "100%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                position: "absolute",
                top: 6,
              }}
            >
              {/* <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "#fb5414",
                  borderRadius: 10,
                  position: "absolute",
                }}
              ></TouchableOpacity> */}
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "#fb5414",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "gray",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 14,
                  width: 14,
                  backgroundColor: "gray",
                  borderRadius: 10,
                }}
              ></TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              height: "15%",
              width: "90%",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View
              style={{
                width: "30%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Share list with store
              </Text>
            </View>
            <View
              style={{
                width: "30%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Response from store
              </Text>
            </View>
            <View
              style={{
                width: "29%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica",
                  color: "#253f67",
                  textAlign: "center",
                }}
              >
                Confirm order
              </Text>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate("OrderDetails")}
            style={{
              height: 33,
              backgroundColor: "#fb5414",
              width: "50%",
              borderRadius: 70,
              justifyContent: "center",
              marginTop: 14,
            }}
          >
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                textAlign: "center",
                fontSize: 15.6,
              }}
            >
              View Details
            </Text>
          </TouchableOpacity>
        </View>

        {/* <View
          style={{
            height: 280,
            width: "90%",
            backgroundColor: "white",
            borderWidth: 1.5,
            borderColor: "#cfe2f3",
            borderRadius: 20,
            elevation: 4,
            alignItems: "center",
            marginTop: 13,
            justifyContent: "center",
          }}
        >
          <View
            style={{
              height: "33%",
              width: "100%",
              //   backgroundColor: "red",
              borderTopStartRadius: 20,
              borderTopEndRadius: 20,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <View style={{ height: "100%", width: "30%" }}>
              <Image
                source={require("../../../LocallyTImages/Shop.png")}
                style={{ height: 60, width: 80, left: 7, top: 11 }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                height: "100%",
                width: "70%",
                alignItems: "flex-end",
              }}
            >
              <View style={{ height: "100%", width: "50%" }}>
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                    top: 11,
                    lineHeight: 24,
                  }}
                >
                  Name_1
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                    top: 11,
                    lineHeight: 24,
                  }}
                >
                  Rating 4.1
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                    top: 11,
                    lineHeight: 24,
                  }}
                >
                  (100+)
                </Text>
              </View>

              <View
                style={{
                  height: "100%",
                  width: "50%",
                  alignItems: "center",
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 18,
                    top: 11,
                    lineHeight: 24,
                  }}
                >
                  Contact
                </Text>
                <TouchableOpacity
                  style={{
                    height: 40,
                    backgroundColor: "#253f67",
                    width: 40,
                    borderRadius: 70,
                    justifyContent: "center",
                    marginTop: 16,
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Wcontact.png")}
                    style={{
                      height: 32,
                      width: 32,
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              height: "10%",
              width: "100%",
              alignItems: "center",
              left: 37,
              bottom: 10,
            }}
          >
            <Text
              style={{
                fontFamily: "Helvetica-Bold",
                color: "#253f67",
                fontSize: 20,
                right: 20,
              }}
            >
              UPI/Wallet, COD
            </Text>
          </View>
          <View
            style={{
              height: "30%",
              width: "90%",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View
              style={{
                width: "30%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Share list with store
              </Text>
            </View>
            <View
              style={{
                width: "30%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Response from store
              </Text>
            </View>
            <View
              style={{
                width: "29%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ fontFamily: "Helvetica", color: "#253f67" }}>
                Confirm order
              </Text>
            </View>
          </View>

          <View
            style={{
              height: "27%",
              width: "100%",
              backgroundColor: "white",
              alignItems: "center",
              justifyContent: "center",
              borderBottomStartRadius: 21,
              borderBottomEndRadius: 21,
            }}
          >
            <TouchableOpacity
              onPress={() => navigation.navigate("OrderDetails")}
              style={{
                height: 40,
                backgroundColor: "#253f67",
                width: 147,
                borderRadius: 70,
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  textAlign: "center",
                  fontSize: 16,
                }}
              >
                View Details
              </Text>
            </TouchableOpacity>
          </View>
        </View> */}
      </View>
    </ScrollView>
  );
}
