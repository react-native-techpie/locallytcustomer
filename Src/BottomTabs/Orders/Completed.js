import React from "react";
import { View, Text, ScrollView, Image, TouchableOpacity } from "react-native";

export default function Completed() {
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{
        backgroundColor: "white",

        height: "100%",
        width: "100%",
      }}
    >
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          marginTop: 35,
          height: "100%",
          paddingBottom: 100,
        }}
      >
        
      </View>
    </ScrollView>
  );
}
