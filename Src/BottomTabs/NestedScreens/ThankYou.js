import React from "react";
import { View, Text, Button, TouchableOpacity, Image } from "react-native";

export default function ThankYou({ navigation }) {
  return (
    <View
      style={{
        flex: 1,
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#253f67",
      }}
    >
      <View
        style={{
          width: "80%",
          alignItems: "center",
          justifyContent: "space-evenly",
          backgroundColor: "white",
          height: "80%",
          borderRadius: 20,
          padding: 20,
        }}
      >
        <Image
          style={{ height: 140, width: 140, tintColor: "#fb5414" }}
          source={require("../../../LocallyTImages/confirm.png")}
        />
        <View style={{ width: "100%", alignItems: "center" }}>
          <Text
            style={{
              fontSize: 17,
              color: "#253f67",
              fontFamily: "Helvetica-Bold",
              lineHeight: 39,
              textAlign: "center",
            }}
          >
            Thank you for your order....
          </Text>
          <Text
            style={{
              fontSize: 17,
              color: "#253f67",
              fontFamily: "Helvetica",
              lineHeight: 27,
              textAlign: "center",
            }}
          >
            Your order was successfully sent to nearest store
          </Text>
        </View>

        <TouchableOpacity
          style={{
            height: 40,
            width: "70%",
            backgroundColor: "#fb5414",
            borderRadius: 39,
            justifyContent: "center",
            marginTop: 25,
          }}
          onPress={() => navigation.navigate("OrderStackScreens")}
        >
          <Text
            style={{
              color: "white",
              fontFamily: "Helvetica-Bold",
              textAlign: "center",
              fontSize: 17,
            }}
          >
            Go to Orders
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
