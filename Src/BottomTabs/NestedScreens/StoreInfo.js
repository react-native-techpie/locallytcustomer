import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  SafeAreaView,
  TextInput,
} from "react-native";
import { Picker } from "@react-native-picker/picker";

const { height, width } = Dimensions.get("window");

export default function StoreInfo({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 14,
        }}
      >
        <View
          style={{
            width: "42%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent: "flex-end",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../../../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "58%",
            alignItems: "flex-end",
            justifyContent: "flex-start",
            flexDirection: "row",
            height: 60,
          }}
        >
          <Image
            style={{ height: 30, width: 30, right: 2, bottom: 3 }}
            source={require("../../../LocallyTImages/location.png")}
          />
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  lineHeight: 17,
                }}
              >
                Delivery to
              </Text>
              <Image
                source={require("../../../LocallyTImages/DropDown.png")}
                style={{ height: 15, width: 15, left: 7, bottom: 2 }}
              />
            </View>
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                fontSize: 12,
                lineHeight: 17,
              }}
            >
              Rose Icon, Pimple Saudagar
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
          height: "88%",
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              width: width,
              alignItems: "center",
            }}
          >
            <View
              style={{
                height: 30,
                width: "90%",
                backgroundColor: "white",
                marginTop: 15,
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: 18,
                  color: "#253f67",
                }}
              >
                Store information
              </Text>
            </View>
            <View
              style={{
                height: 242,
                width: "90%",
                backgroundColor: "white",
                borderWidth: 1.5,
                borderColor: "#cfe2f3",
                borderRadius: 20,
                elevation: 1,
                alignItems: "center",
              }}
            >
              <View
                style={{
                  height: "50%",
                  width: "100%",
                  //   backgroundColor: "red",
                  borderTopStartRadius: 20,
                  borderTopEndRadius: 20,
                  flexDirection: "row",
                }}
              >
                <View
                  style={{ height: "100%", width: "27%", alignItems: "center" }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 55, width: 70, left: 1, top: 11 }}
                  />
                </View>
                <View
                  style={{
                    width: "73%",
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                    }}
                  >
                    <View>
                      <Text
                        style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}
                      >
                        Venus super market
                      </Text>
                      <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                        Thergaon | 2.5 kms
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <TouchableOpacity
                        style={{
                          height: 30,
                          backgroundColor: "#253f67",
                          width: 30,
                          borderRadius: 70,
                          justifyContent: "center",
                          marginTop: 6,
                        }}
                      >
                        <Image
                          source={require("../../../LocallyTImages/Wcontact.png")}
                          style={{
                            height: 25,
                            width: 25,
                            alignSelf: "center",
                          }}
                        />
                      </TouchableOpacity>
                      <Image
                        source={require("../../../LocallyTImages/location.png")}
                        style={{
                          height: 27,
                          width: 27,
                          alignSelf: "center",
                          tintColor: "#fb5414",
                          left: 5,
                        }}
                      />
                    </View>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                      borderBottomWidth: 1.3,
                      borderColor: "gray",
                    }}
                  >
                    <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                      Delivery:1 Hrs | No min order value
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                      height: 50,
                      alignItems: "center",
                    }}
                  >
                    <View
                      style={{
                        width: "100%",
                        // justifyContent: "center",
                        height: 50,
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <Image
                        source={require("../../../LocallyTImages/Percent.png")}
                        style={{
                          height: 29,
                          width: 29,
                        }}
                      />
                      <View
                        style={{
                          height: "100%",
                          // alignItems: "center",
                          justifyContent: "center",
                          width: "80%",
                          left: 4,
                        }}
                      >
                        <Text
                          style={{ fontSize: 11.5, fontFamily: "Helvetica" }}
                        >
                          10% off on orders above Rs. 500
                        </Text>
                        <Text style={{ fontSize: 11, fontFamily: "Helvetica" }}>
                          15% off on orders above Rs. 1000
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{ height: "50%", width: "90%" }}>
                <Text style={{ fontSize: 17, fontFamily: "Helvetica-Bold" }}>
                  Store Images
                </Text>
                <ScrollView
                  style={{
                    height: 70,
                    marginTop: 10,
                    width: "100%",
                    backgroundColor: "white",
                  }}
                  horizontal
                >
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 70, width: 88, marginLeft: 1 }}
                  />
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 70, width: 88, marginLeft: 15 }}
                  />
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 70, width: 88, marginLeft: 15 }}
                  />
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 70, width: 88, marginLeft: 15 }}
                  />
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 70, width: 88, marginLeft: 15 }}
                  />
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 70, width: 88, marginLeft: 15 }}
                  />
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 70, width: 88, marginLeft: 15 }}
                  />
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 70, width: 88, marginLeft: 15 }}
                  />
                </ScrollView>
              </View>
            </View>
            <View
              style={{
                height: 40,
                width: "90%",
                backgroundColor: "white",
                marginTop: 15,
              }}
            >
              <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 16 }}>
                Make your payment to the store using following-
              </Text>
            </View>
            <View
              style={{
                width: "90%",
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
                marginTop: 11,
              }}
            >
              <Text
                style={{
                  color: "#253f67",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 17,
                }}
              >
                9912457896
              </Text>
              <TouchableOpacity
                style={{
                  height: 27,
                  justifyContent: "center",
                  width: "25%",
                  borderRadius: 29,
                  left: 11,
                  backgroundColor: "#253f67",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    lineHeight: 15,
                    textAlign: "center",
                    fontFamily: "Helvetica",
                  }}
                >
                  Copy
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: "90%",
                height: 54,
                alignItems: "center",
                justifyContent: "space-between",
                flexDirection: "row",
              }}
            >
              <TouchableOpacity
                style={{
                  height: 32,
                  width: "24%",
                  borderRadius: 16,
                  backgroundColor: "white",
                  marginTop: 16,
                  borderWidth: 2,
                  borderColor: "#cfe2f3",
                  justifyContent: "center",
                  paddingLeft: 10,
                  alignItems: "center",
                }}
              >
                <Image
                  source={require("../../../LocallyTImages/PayTm.jpg")}
                  style={{ height: 24, width: 61, right: 3 }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 32,
                  width: "24%",
                  borderRadius: 16,
                  backgroundColor: "white",
                  marginTop: 16,
                  borderWidth: 2,
                  borderColor: "#cfe2f3",
                  justifyContent: "center",
                  paddingLeft: 10,
                  alignItems: "center",
                }}
              >
                <Image
                  source={require("../../../LocallyTImages/GooglePay.jpg")}
                  style={{ height: 24, width: 61, right: 3 }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 32,
                  width: "24%",
                  borderRadius: 16,
                  backgroundColor: "white",
                  marginTop: 16,
                  borderWidth: 2,
                  borderColor: "#cfe2f3",
                  justifyContent: "center",
                  paddingLeft: 10,
                  alignItems: "center",
                }}
              >
                <Image
                  source={require("../../../LocallyTImages/PhonePay.jpg")}
                  style={{ height: 24, width: 61, right: 3 }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  height: 32,
                  width: "24%",
                  borderRadius: 16,
                  backgroundColor: "white",
                  marginTop: 16,
                  borderWidth: 2,
                  borderColor: "#cfe2f3",
                  justifyContent: "center",
                  paddingLeft: 10,
                  alignItems: "center",
                }}
              >
                <Image
                  source={require("../../../LocallyTImages/Bhim.png")}
                  style={{ height: 25, width: 63, right: 3 }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                width: "90%",
                alignItems: "center",
                justifyContent: "space-between",
                flexDirection: "row",
                marginTop: 15,
              }}
            >
              <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 16 }}>
                UPI ID for payment
              </Text>
            </View>
            <View
              style={{
                width: "90%",
                alignItems: "center",
                flexDirection: "row",
                marginTop: 0,
              }}
            >
              <Image
                source={require("../../../LocallyTImages/UPI.jpg")}
                style={{ height: 55, width: 93, right: 3 }}
              />
              <Text
                style={{
                  color: "#253f67",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 17,
                  left: 51,
                }}
              >
                abc@sbi
              </Text>
              <TouchableOpacity
                style={{
                  height: 27,
                  justifyContent: "center",
                  width: "25%",
                  borderRadius: 29,
                  left: 69,
                  backgroundColor: "#253f67",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    lineHeight: 15,
                    textAlign: "center",
                    fontFamily: "Helvetica",
                  }}
                >
                  Copy
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: 30,
                width: "90%",
                backgroundColor: "white",
                marginTop: 15,
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: 17,
                  color: "#253f67",
                }}
              >
                QR code images
              </Text>
            </View>
            <ScrollView
              style={{
                height: 100,
                marginTop: 10,
                width: "90%",
                borderWidth: 1,
                backgroundColor: "white",
                borderRadius: 6,
              }}
              horizontal
            ></ScrollView>
          </View>
          <View
            style={{
              height: 10,
              width: width,
              backgroundColor: "white",
              marginTop: 5,
            }}
          ></View>
        </ScrollView>
      </View>
      <View
        style={{ height: 5, width: width, backgroundColor: "white" }}
      ></View>

      <View
        style={{ height: 20, width: width, backgroundColor: "white" }}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({});

// import React, { useState } from "react";
// import {
//   View,
//   Text,
//   StyleSheet,
//   Image,
//   Dimensions,
//   TouchableOpacity,
//   TouchableWithoutFeedback,
//   Keyboard,
//   ScrollView,
//   SafeAreaView,
//   TextInput,
// } from "react-native";
// import { Picker } from "@react-native-picker/picker";

// const { height, width } = Dimensions.get("window");

// export default function StoreInfo({ navigation }) {
//   const [selectedValue, setSelectedValue] = useState("Pune");

//   return (
//     <View
//       style={{
//         width: width,
//         alignItems: "center",
//         justifyContent: "flex-end",
//         backgroundColor: "#253f67",
//         flex:1
//       }}
//     >
//       <View
//         style={{
//           width: width,
//           height: '20%',
//           backgroundColor: "transparent",
//           alignItems: "center",
//           flexDirection: "row",
//           paddingTop: 10,
//         }}
//       >
//         <View
//           style={{
//             width: "60%",
//             paddingLeft: 20,
//             alignItems: "flex-start",
//             height: 50,
//           }}
//         >
//           <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
//             <Image
//               style={{ height: 46, width: 46 }}
//               source={require("../../../LocallyTImages/profile.png")}
//             />
//           </TouchableOpacity>
//         </View>

//         <View
//           style={{
//             width: "40%",
//             paddingLeft: 20,
//             alignItems: "center",
//             height: 50,
//             paddingRight: 20,
//             justifyContent: "center",
//             flexDirection: "row",
//           }}
//         >
//           <Image
//             style={{ height: 30, width: 30 }}
//             source={require("../../../LocallyTImages/location.png")}
//           />
//           <Picker
//             style={{ height: 50, width: "100%", color: "white" }}
//             dropdownIconColor="white"
//             selectedValue={selectedValue}
//             onValueChange={(itemValue, itemIndex) =>
//               setSelectedValue(itemValue)
//             }
//           >
//             <Picker.Item label="Pune" value="Pune" />
//             <Picker.Item label="Mumbai" value="Mumbai" />
//             <Picker.Item label="Nashik" value="Nashik" />
//             <Picker.Item label="Kolhapur" value="Kolhapur" />
//           </Picker>
//         </View>
//       </View>
//       <View
//         style={{
//           width: "100%",
//           alignItems: "center",
//           justifyContent: "center",
//           backgroundColor: "white",
//           borderTopStartRadius: 25,
//           borderTopEndRadius: 25,
//           height:'75%'
//         }}
//       >
//         <ScrollView showsVerticalScrollIndicator={false}>
//           <View
//             style={{
//               width: width,
//               alignItems: "center",
//             }}
//           >
//           <View
//             style={{
//               height: 30,
//               width: "90%",
//               backgroundColor: "white",
//               marginTop: 25,
//             }}
//           >
//             <Text
//               style={{
//                 fontFamily: "Helvetica-Bold",
//                 fontSize: 18,
//                 color: "#253f67",
//               }}
//             >
//               Store information
//             </Text>
//           </View>
//           <View
//             style={{
//               height: 300,
//               width: "90%",
//               backgroundColor: "white",
//               borderWidth: 1.5,
//               borderColor: "#cfe2f3",
//               borderRadius: 20,
//               elevation: 4,
//               alignItems: "center",
//               marginTop: 10,
//             }}
//           >
//             <View
//               style={{
//                 height: "37%",
//                 width: "100%",
//                 //   backgroundColor: "red",
//                 borderTopStartRadius: 20,
//                 borderTopEndRadius: 20,
//                 flexDirection: "row",
//                 alignItems: "center",
//                 justifyContent: "space-between",
//                 marginTop: 6,
//               }}
//             >
//               <View style={{ height: "100%", width: "30%" }}>
//                 <Image
//                   source={require("../../../LocallyTImages/Shop.png")}
//                   style={{ height: 60, width: 80, left: 7, top: 11 }}
//                 />
//               </View>
//               <View
//                 style={{
//                   flexDirection: "row",
//                   height: "100%",
//                   width: "70%",
//                   alignItems: "flex-end",
//                 }}
//               >
//                 <View style={{ height: "100%", width: "40%" }}>
//                   <Text
//                     style={{
//                       fontFamily: "Helvetica-Bold",
//                       fontSize: 14,
//                       top: 11,
//                       lineHeight: 20,
//                     }}
//                   >
//                     Name_1
//                   </Text>
//                   <Text
//                     style={{
//                       fontFamily: "Helvetica-Bold",
//                       fontSize: 14,
//                       top: 11,
//                       lineHeight: 20,
//                     }}
//                   >
//                     Rating 4.1
//                   </Text>
//                   <Text
//                     style={{
//                       fontFamily: "Helvetica-Bold",
//                       fontSize: 14,
//                       top: 11,
//                       lineHeight: 20,
//                     }}
//                   >
//                     (100+)
//                   </Text>
//                 </View>
//                 <View style={{ height: "100%", width: "60%" }}>
//                   <Text
//                     style={{
//                       fontFamily: "Helvetica-Bold",
//                       fontSize: 13,
//                       top: 11,
//                       lineHeight: 20,
//                     }}
//                   >
//                     Delivers in 1hrs
//                   </Text>
//                   <View
//                     style={{
//                       flexDirection: "row",
//                       alignItems: "center",
//                       justifyContent: "space-between",
//                       width: "80%",
//                     }}
//                   >
//                     <Text
//                       style={{
//                         fontFamily: "Helvetica-Bold",
//                         fontSize: 13,
//                         top: 9,
//                         lineHeight: 30,
//                       }}
//                     >
//                       Store location
//                     </Text>
//                     <Image
//                       source={require("../../../LocallyTImages/location.png")}
//                       style={{
//                         height: 23,
//                         width: 23,
//                         marginTop: 17,
//                         tintColor: "#fb5414",
//                         left: 9,
//                       }}
//                     />
//                   </View>
//                   <View
//                     style={{
//                       flexDirection: "row",
//                       alignItems: "center",
//                       justifyContent: "space-between",
//                       width: "90%",
//                     }}
//                   >
//                     <Text
//                       style={{
//                         fontFamily: "Helvetica-Bold",
//                         fontSize: 13,
//                         top: 4,
//                         lineHeight: 20,
//                       }}
//                     >
//                       Contact shop
//                     </Text>
//                     <TouchableOpacity
//                       // onPress={()=>navigation.navigate("ChooseStore")}
//                       style={{
//                         height: 27,
//                         backgroundColor: "#253f67",
//                         width: 27,
//                         borderRadius: 70,
//                         justifyContent: "center",
//                         top: 5,
//                       }}
//                     >
//                       <Image
//                         source={require("../../../LocallyTImages/Wcontact.png")}
//                         style={{
//                           height: 22,
//                           width: 22,
//                           alignSelf: "center",
//                         }}
//                       />
//                     </TouchableOpacity>
//                   </View>
//                 </View>
//               </View>
//             </View>
//             <View style={{ width: "90%", height: "10%", marginTop: 10 }}>
//               <Text
//                 style={{
//                   fontFamily: "Helvetica-Bold",
//                   fontSize: 16,
//                   top: 4,
//                   lineHeight: 20,
//                   color: "#253f67",
//                 }}
//               >
//                 Phone number for payment
//               </Text>
//             </View>
//             <View
//               style={{
//                 width: "90%",
//                 height: "10%",
//                 marginTop: 10,
//                 flexDirection: "row",
//               }}
//             >
//               <View style={{ width: "50%" }}>
//                 <Text
//                   style={{
//                     fontFamily: "Helvetica-Bold",
//                     fontSize: 16,
//                     top: 4,
//                     lineHeight: 20,
//                     color: "#253f67",
//                   }}
//                 >
//                   +91 9912345678
//                 </Text>
//               </View>
//               <View style={{ width: "50%" }}>
//                 <TouchableOpacity
//                   style={{
//                     height: 30,
//                     width: 130,
//                     borderRadius: 20,
//                     backgroundColor: "#253f67",
//                     alignItems: "center",
//                     justifyContent: "center",
//                   }}
//                 >
//                   <Text style={{ color: "white", fontFamily: "Helvetica" }}>
//                     Tap to copy
//                   </Text>
//                 </TouchableOpacity>
//               </View>
//             </View>
//             <View style={{ width: "90%", height: "10%", marginTop: 10 }}>
//               <Text
//                 style={{
//                   fontFamily: "Helvetica-Bold",
//                   fontSize: 16,
//                   top: 4,
//                   lineHeight: 20,
//                   color: "#253f67",
//                 }}
//               >
//                 UPI ID for payment
//               </Text>
//             </View>
//             <View
//               style={{
//                 width: "90%",
//                 height: "10%",
//                 marginTop: 10,
//                 flexDirection: "row",
//               }}
//             >
//               <View style={{ width: "50%" }}>
//                 <Text
//                   style={{
//                     fontFamily: "Helvetica-Bold",
//                     fontSize: 16,
//                     top: 4,
//                     lineHeight: 20,
//                     color: "#253f67",
//                   }}
//                 >
//                   abc@sbi
//                 </Text>
//               </View>
//               <View style={{ width: "50%" }}>
//                 <TouchableOpacity
//                   style={{
//                     height: 30,
//                     width: 130,
//                     borderRadius: 20,
//                     backgroundColor: "#253f67",
//                     alignItems: "center",
//                     justifyContent: "center",
//                   }}
//                 >
//                   <Text style={{ color: "white", fontFamily: "Helvetica" }}>
//                     Tap to copy
//                   </Text>
//                 </TouchableOpacity>
//               </View>
//             </View>
//           </View>
//           <View
//             style={{
//               height: 30,
//               width: "90%",
//               backgroundColor: "white",
//               marginTop: 30,
//             }}
//           >
//             <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 16 }}>
//               Accepted modes of payment
//             </Text>
//           </View>
//           <View
//             style={{
//               width: "90%",
//               height: 54,
//               alignItems: "center",
//               justifyContent: "space-between",
//               flexDirection: "row",
//             }}
//           >
//             <TouchableOpacity
//               style={{
//                 height: 49,
//                 width: "46%",
//                 borderRadius: 16,
//                 backgroundColor: "white",
//                 marginTop: 16,
//                 borderWidth: 2,
//                 borderColor: "#cfe2f3",
//                 justifyContent: "center",
//                 paddingLeft: 10,
//                 alignItems: "center",
//               }}
//             >
//               <Image
//                 source={require("../../../LocallyTImages/PayTm.jpg")}
//                 style={{ height: 34, width: 100 }}
//               />
//             </TouchableOpacity>
//             <TouchableOpacity
//               style={{
//                 height: 49,
//                 width: "46%",
//                 borderRadius: 16,
//                 backgroundColor: "white",
//                 marginTop: 16,
//                 borderWidth: 2,
//                 borderColor: "#cfe2f3",
//                 justifyContent: "center",
//                 paddingLeft: 10,
//                 alignItems: "center",
//               }}
//             >
//               <Image
//                 source={require("../../../LocallyTImages/PhonePay.jpg")}
//                 style={{ height: 40, width: 120 }}
//               />
//             </TouchableOpacity>
//           </View>

//           <View
//             style={{
//               width: "90%",
//               height: 54,
//               alignItems: "center",
//               justifyContent: "space-between",
//               flexDirection: "row",
//               marginTop: 10,
//             }}
//           >
//             <TouchableOpacity
//               style={{
//                 height: 49,
//                 width: "46%",
//                 borderRadius: 16,
//                 backgroundColor: "white",
//                 marginTop: 16,
//                 borderWidth: 2,
//                 borderColor: "#cfe2f3",
//                 justifyContent: "center",
//                 paddingLeft: 10,
//                 alignItems: "center",
//               }}
//             >
//               <Image
//                 source={require("../../../LocallyTImages/GooglePay.jpg")}
//                 style={{ height: 25, width: 98 }}
//               />
//             </TouchableOpacity>
//             <TouchableOpacity
//               style={{
//                 height: 49,
//                 width: "46%",
//                 borderRadius: 16,
//                 backgroundColor: "white",
//                 marginTop: 16,
//                 borderWidth: 2,
//                 borderColor: "#cfe2f3",
//                 justifyContent: "center",
//                 paddingLeft: 10,
//                 alignItems: "center",
//               }}
//             >
//               <Image
//                 source={require("../../../LocallyTImages/UPI.jpg")}
//                 style={{ height: 30, width: 97 }}
//               />
//             </TouchableOpacity>
//           </View>
//           <Text
//             style={{
//               marginTop: 13,
//               fontFamily: "Helvetica-Bold",
//               fontSize: 17,
//             }}
//           >
//             OR
//           </Text>
//           <TouchableOpacity style={{ marginTop: 7 }}>
//             <Text
//               style={{
//                 fontFamily: "Helvetica-Bold",
//                 fontSize: 17,
//                 color: "#253f67",
//               }}
//             >
//               pay using Cash
//             </Text>
//           </TouchableOpacity>
//           <View
//             style={{
//               height: 30,
//               width: "90%",
//               backgroundColor: "white",
//               marginTop: 25,
//             }}
//           >
//             <Text
//               style={{
//                 fontFamily: "Helvetica-Bold",
//                 fontSize: 17,
//                 color: "#253f67",
//               }}
//             >
//               Store Images
//             </Text>
//           </View>
//           <ScrollView
//             style={{
//               height: 110,
//               marginTop: 10,
//               width: "100%",
//               backgroundColor: "white",
//             }}
//             horizontal
//           >
//             <Image
//               source={require("../../../LocallyTImages/Shop.png")}
//               style={{ height: 100, width: 128, marginLeft: 15 }}
//             />
//             <Image
//               source={require("../../../LocallyTImages/Shop.png")}
//               style={{ height: 100, width: 128, marginLeft: 15 }}
//             />
//             <Image
//               source={require("../../../LocallyTImages/Shop.png")}
//               style={{ height: 100, width: 128, marginLeft: 15 }}
//             />
//             <Image
//               source={require("../../../LocallyTImages/Shop.png")}
//               style={{ height: 100, width: 128, marginLeft: 15 }}
//             />
//             <Image
//               source={require("../../../LocallyTImages/Shop.png")}
//               style={{ height: 100, width: 128, marginLeft: 15 }}
//             />
//             <Image
//               source={require("../../../LocallyTImages/Shop.png")}
//               style={{ height: 100, width: 128, marginLeft: 15 }}
//             />
//             <Image
//               source={require("../../../LocallyTImages/Shop.png")}
//               style={{ height: 100, width: 128, marginLeft: 15 }}
//             />
//             <Image
//               source={require("../../../LocallyTImages/Shop.png")}
//               style={{ height: 100, width: 128, marginLeft: 15 }}
//             />
//           </ScrollView>
//         </View>
//         <View
//           style={{
//             height: 8,
//             width: width,
//             backgroundColor: "white",
//             marginTop: 5,
//           }}
//         ></View>
//         </ScrollView>
//         </View>
//       <View
//         style={{ height: 20, width: width, backgroundColor: "white" }}
//       ></View>

//       <View
//         style={{ height: 20, width: width, backgroundColor: "white" }}
//       ></View>
//     </View>
//   );
// }

// const styles = StyleSheet.create({});
