import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  SafeAreaView,
  TextInput,
} from "react-native";
import { Picker } from "@react-native-picker/picker";

const { height, width } = Dimensions.get("window");

export default function ChooseStore({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 14,
        }}
      >
        <View
          style={{
            width: "42%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent: "flex-end",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../../../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "58%",
            alignItems: "flex-end",
            justifyContent: "flex-start",
            flexDirection: "row",
            height: 60,
          }}
        >
          <Image
            style={{ height: 30, width: 30, right: 2, bottom: 3 }}
            source={require("../../../LocallyTImages/location.png")}
          />
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  lineHeight: 17,
                }}
              >
                Delivery to
              </Text>
              <Image
                source={require("../../../LocallyTImages/DropDown.png")}
                style={{ height: 15, width: 15, left: 7, bottom: 2 }}
              />
            </View>
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                fontSize: 12,
                lineHeight: 17,
              }}
            >
              Rose Icon, Pimple Saudagar
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
          height: "85%",
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              width: width,
              alignItems: "center",
            }}
          >
            <View style={{ width: "90%", marginTop: 15, alignItems: "center" }}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Helvetica",
                  lineHeight: 20,
                }}
              >
                Got your list!
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Helvetica",
                  lineHeight: 20,
                }}
              >
                Select one of the following options to
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Helvetica",
                  lineHeight: 20,
                }}
              >
                share your order with nearby stores
              </Text>
            </View>
            <View
              style={{
                width: "90%",
                height: 205,
                borderWidth: 1.9,
                marginTop: 15,
                borderStyle: "dashed",
                borderRadius: 1,
                justifyContent: "space-around",
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "40%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../../../LocallyTImages/Graph.png")}
                  style={{ height: 80, width: 300 }}
                />
              </View>
              <View
                style={{
                  width: "100%",
                  height: "30%",
                  alignItems: "center",
                  marginTop: 10,
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    color: "#253f67",
                    fontFamily: "Helvetica",
                    lineHeight: 17,
                  }}
                >
                  Your list will be shared with 3 nearest stores.
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: "#253f67",
                    fontFamily: "Helvetica",
                    lineHeight: 17,
                  }}
                >
                  Once stores respond, confirm your order with the store
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: "#253f67",
                    fontFamily: "Helvetica",
                    lineHeight: 17,
                  }}
                >
                  offering best value to you
                </Text>
              </View>
              <View
                style={{
                  width: "100%",
                  height: "30%",
                  alignItems: "center",
                  marginTop: 10,
                }}
              >
                <TouchableOpacity
                  onPress={() => navigation.navigate("ThankYou")}
                  style={{
                    height: 35,
                    width: 200,
                    backgroundColor: "#fb5414",
                    borderRadius: 20,
                    justifyContent: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      textAlign: "center",
                      fontFamily: "Helvetica",
                    }}
                  >
                    Send to nearest stores
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                width: "90%",
                height: 29,
                marginTop: 10,
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
              }}
            >
              <View
                style={{
                  height: 1,
                  width: "45%",
                  borderWidth: 1,
                  borderColor: "#fb4514",
                }}
              ></View>
              <View
                style={{
                  height: 29,
                  width: "10%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 15 }}>
                  OR
                </Text>
              </View>
              <View
                style={{
                  height: 1,
                  width: "45%",
                  borderWidth: 1,
                  borderColor: "#fb4514",
                }}
              ></View>
            </View>

            <View
              style={{
                width: "90%",
                height: 205,
                borderWidth: 1.9,
                marginTop: 10,
                borderStyle: "dashed",
                borderRadius: 1,
                justifyContent: "space-around",
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "40%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../../../LocallyTImages/Graph1.png")}
                  style={{ height: 77, width: 300 }}
                />
              </View>
              <View
                style={{
                  width: "100%",
                  height: "22%",
                  alignItems: "center",
                  marginTop: 10,
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    color: "#253f67",
                    fontFamily: "Helvetica",
                    lineHeight: 17,
                  }}
                >
                  Select your favorite store from the list.
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: "#253f67",
                    fontFamily: "Helvetica",
                    lineHeight: 17,
                  }}
                >
                  Once stores respond, confirm your order with the store.
                </Text>
              </View>
              <View
                style={{
                  width: "100%",
                  height: "30%",
                  alignItems: "center",
                  marginTop: 10,
                }}
              >
                <TouchableOpacity
                  onPress={() => navigation.navigate("ThankYou")}
                  style={{
                    height: 35,
                    width: 200,
                    backgroundColor: "#fb5414",
                    borderRadius: 20,
                    justifyContent: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      textAlign: "center",
                      fontFamily: "Helvetica",
                    }}
                  >
                    Choose store
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
      <View
        style={{ height: 20, width: width, backgroundColor: "white" }}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({});
