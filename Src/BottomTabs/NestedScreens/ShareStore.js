import React, {useState} from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import { Picker } from "@react-native-picker/picker";

const { height, width } = Dimensions.get("window");

export default function ShareStore({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 14,
        }}
      >
        <View
          style={{
            width: "42%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent: "flex-end",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../../../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "58%",
            alignItems: "flex-end",
            justifyContent: "flex-start",
            flexDirection: "row",
            height: 60,
          }}
        >
          <Image
            style={{ height: 30, width: 30, right: 2, bottom: 3 }}
            source={require("../../../LocallyTImages/location.png")}
          />
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  lineHeight: 17,
                }}
              >
                Delivery to
              </Text>
              <Image
                source={require("../../../LocallyTImages/DropDown.png")}
                style={{ height: 15, width: 15, left: 7, bottom: 2 }}
              />
            </View>
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                fontSize: 12,
                lineHeight: 17,
              }}
            >
              Rose Icon, Pimple Saudagar
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
          height: "85%",
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              width: width,
              alignItems: "center",
            }}
          >
            <View style={{ width: "90%", marginTop: 15, paddingBottom: 2 }}>
              <Text
                style={{
                  color: "#253f67",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 17,
                }}
              >
                Tap on the name of the store you want to order from
              </Text>

              <View
                style={{
                  height: 185,
                  width: "100%",
                  backgroundColor: "white",
                  elevation: 2,
                  borderRadius: 20,
                  marginTop: 10,
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    height: "100%",
                    width: "30%",
                    backgroundColor: "pink",
                    borderTopStartRadius: 20,
                    borderBottomStartRadius: 20,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 46, width: 60 }}
                  />
                </View>
                <View
                  style={{
                    height: "100%",
                    width: "70%",
                    backgroundColor: "white",
                    borderTopEndRadius: 21,
                    borderBottomEndRadius: 21,
                    paddingLeft: 3,
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                      marginTop: 4,
                    }}
                  >
                    <Text
                      style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}
                    >
                      Venus super market
                    </Text>
                    <Image
                      source={require("../../../LocallyTImages/heart.jpg")}
                      style={{ height: 25, width: 25, right: 9 }}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                    }}
                  >
                    <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                      Thergaon | 2.5 kms
                    </Text>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        height: 20,
                      }}
                    >
                      <Image
                        source={require("../../../LocallyTImages/star.png")}
                        style={{ height: 15, width: 15, right: 6 }}
                      />
                      <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                        3.9
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                      borderBottomWidth: 1.3,
                      borderColor: "gray",
                    }}
                  >
                    <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                      Delivery:1 Hrs | No min order value
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                      height: 50,
                      alignItems: "center",
                    }}
                  >
                    <View
                      style={{
                        width: "100%",
                        // justifyContent: "center",
                        height: 50,
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <Image
                        source={require("../../../LocallyTImages/Percent.png")}
                        style={{
                          height: 29,
                          width: 29,
                        }}
                      />
                      <View
                        style={{
                          height: "100%",
                          // alignItems: "center",
                          justifyContent: "center",
                          width: "80%",
                          marginLeft: 5,
                        }}
                      >
                        <Text style={{ fontSize: 11, fontFamily: "Helvetica" }}>
                          10% off on orders above Rs. 500
                        </Text>
                        <Text style={{ fontSize: 11, fontFamily: "Helvetica" }}>
                          15% off on orders above Rs. 1000
                        </Text>
                      </View>
                    </View>
                  </View>

                  <View
                    style={{
                      height: "30%",
                      width: "100%",
                      alignItems: "center",
                      marginTop: 0,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => navigation.navigate("ChooseStore")}
                      style={{
                        height: 35,
                        width: "55%",
                        backgroundColor: "#fb5414",
                        borderRadius: 30,
                        justifyContent: "center",
                        marginTop: 5,
                      }}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "Helvetica",
                          textAlign: "center",
                          fontSize: 16,
                        }}
                      >
                        Share list
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              <View
                style={{ height: 20, width: "100%", backgroundColor: "white" }}
              ></View>
            </View>
          </View>
        </ScrollView>
      </View>
      <View
        style={{ height: 20, width: "100%", backgroundColor: "white" }}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({});
