import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  SafeAreaView,
  TextInput,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Picker } from "@react-native-picker/picker";

const { height, width } = Dimensions.get("window");

export default function OrderDetails({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 14,
        }}
      >
        <View
          style={{
            width: "42%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent: "flex-end",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../../../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "58%",
            alignItems: "flex-end",
            justifyContent: "flex-start",
            flexDirection: "row",
            height: 60,
          }}
        >
          <Image
            style={{ height: 30, width: 30, right: 2, bottom: 3 }}
            source={require("../../../LocallyTImages/location.png")}
          />
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  lineHeight: 17,
                }}
              >
                Delivery to
              </Text>
              <Image
                source={require("../../../LocallyTImages/DropDown.png")}
                style={{ height: 15, width: 15, left: 7, bottom: 2 }}
              />
            </View>
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                fontSize: 12,
                lineHeight: 17,
              }}
            >
              Rose Icon, Pimple Saudagar
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
          height: "78.4%",
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              width: width,
              alignItems: "center",
            }}
          >
            <View
              style={{
                height: 123,
                width: "90%",
                backgroundColor: "white",
                borderWidth: 1.5,
                borderColor: "#cfe2f3",
                borderRadius: 20,
                elevation: 1,
                alignItems: "center",
                marginTop: 15,
              }}
            >
              <View
                style={{
                  height: "50%",
                  width: "100%",
                  //   backgroundColor: "red",
                  borderTopStartRadius: 20,
                  borderTopEndRadius: 20,
                  flexDirection: "row",
                }}
              >
                <View style={{ height: "100%", width: "30%" }}>
                  <Image
                    source={require("../../../LocallyTImages/Shop.png")}
                    style={{ height: 60, width: 80, left: 7, top: 11 }}
                  />
                </View>
                <View
                  style={{
                    width: "70%",
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                    }}
                  >
                    <View>
                      <Text
                        style={{ fontSize: 16, fontFamily: "Helvetica-Bold" }}
                      >
                        Venus super market
                      </Text>
                      <Text style={{ fontSize: 13, fontFamily: "Helvetica" }}>
                        Thergaon | 2.5 kms
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={{
                        height: 33,
                        backgroundColor: "#253f67",
                        width: 33,
                        borderRadius: 70,
                        justifyContent: "center",
                        marginTop: 6,
                      }}
                    >
                      <Image
                        source={require("../../../LocallyTImages/Wcontact.png")}
                        style={{
                          height: 27,
                          width: 27,
                          alignSelf: "center",
                        }}
                      />
                    </TouchableOpacity>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                      borderBottomWidth: 1.3,
                      borderColor: "gray",
                    }}
                  >
                    <Text style={{ fontSize: 12, fontFamily: "Helvetica" }}>
                      Delivery:1 Hrs | No min order value
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      width: "95%",
                      justifyContent: "space-between",
                      height: 50,
                      alignItems: "center",
                    }}
                  >
                    <View
                      style={{
                        width: "100%",
                        // justifyContent: "center",
                        height: 50,
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <Image
                        source={require("../../../LocallyTImages/Percent.png")}
                        style={{
                          height: 29,
                          width: 29,
                        }}
                      />
                      <View
                        style={{
                          height: "100%",
                          alignItems: "center",
                          justifyContent: "center",
                          width: "80%",
                          left: 4,
                        }}
                      >
                        <Text
                          style={{ fontSize: 11.5, fontFamily: "Helvetica" }}
                        >
                          10% off on orders above Rs. 500
                        </Text>
                        <Text style={{ fontSize: 11, fontFamily: "Helvetica" }}>
                          15% off on orders above Rs. 1000
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View
              style={{
                height: 30,
                width: "90%",
                backgroundColor: "white",
                marginTop: 10,
              }}
            >
              <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 16 }}>
                Order Details
              </Text>
            </View>
            <View
              style={{
                height: 230,
                width: "90%",
                backgroundColor: "#FADBD8",
                borderRadius: 25,
                alignItems: "center",
                justifyContent:'space-evenly'
              }}
            >
              <View
                style={{
                  height: "40%",
                  width: "100%",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-evenly",
                }}
              >
                <Icon name="chevron-left" size={24} color="#283673" />
                <View
                  style={{
                    height: 80,
                    borderRadius: 20,
                    borderWidth: 1.5,
                    borderColor: "gray",
                    width: 80,
                    backgroundColor: "white",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/grocery.png")}
                    style={{ height: 55, width: 65 }}
                  />
                </View>
                <View
                  style={{
                    height: 80,
                    borderRadius: 20,
                    borderWidth: 1.5,
                    borderColor: "gray",
                    width: 80,
                    backgroundColor: "white",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Image
                    source={require("../../../LocallyTImages/grocery.png")}
                    style={{ height: 55, width: 65 }}
                  />
                </View>
                <Icon name="chevron-right" size={24} color="#283673" />
              </View>
              <View
                style={{
                  height: "15%",
                  width: "100%",
                  flexDirection: "row",
                }}
              >
                <View
                  style={{
                    height: "100%",
                    width: "100%",
                    paddingLeft: 10,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 17 }}>
                    All Items are available....
                  </Text>
                  {/* <Text style={{ fontFamily: "Helvetica" }}>
                  (only if any item is not available)
                </Text> */}
                </View>
                {/* <View
                  style={{
                    height: "100%",
                    width: "30%",
                    paddingLeft: 10,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      color: "#253f67",
                      fontSize: 13,
                    }}
                  >
                    New value 1
                  </Text>

                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      color: "#253f67",
                      fontSize: 13,
                    }}
                  >
                    New value 2
                  </Text>
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      color: "#253f67",
                      fontSize: 13,
                    }}
                  >
                    New value 3
                  </Text>
                </View> */}
              </View>
              <View
                style={{
                  height: "35%",
                  width: "70%",
                  flexDirection: "row",
                }}
              >
                <View
                  style={{
                    height: "100%",
                    width: "53%",
                    justifyContent: "center",
                  }}
                >
                  <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 15 }}>
                    Total price
                  </Text>
                  <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 15 }}>
                    Delivery charge
                  </Text>
                  <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 15 }}>
                    Total payble
                  </Text>
                </View>
                <View
                  style={{
                    height: "100%",
                    width: "47%",
                    justifyContent: "center",
                    paddingLeft: 10,
                  }}
                >
                  <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 15 }}>
                    Rs xxx.xx
                  </Text>
                  <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 15 }}>
                    Rs xx.00
                  </Text>
                  <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 15 }}>
                    Rs xxx.xx
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                height: 20,
                width: "90%",
                backgroundColor: "white",
                marginTop: 10,
              }}
            >
              <Text style={{ fontFamily: "Helvetica-Bold", fontSize: 17 }}>
                Comments
              </Text>
            </View>
            <TextInput
              style={{
                height: 52,
                width: "90%",
                backgroundColor: "white",
                borderWidth: 1.5,
                borderColor: "#cfe2f3",
                borderRadius: 16,
                // elevation: 1,
                marginTop: 11,
                paddingLeft: 11,
              }}
              placeholder="User comments here..."
              multiline
            />
          </View>
          <View
            style={{
              height: 8,
              width: width,
              backgroundColor: "white",
              marginTop: 5,
            }}
          ></View>
        </ScrollView>
      </View>
      <View
        style={{ height: 10, width: width, backgroundColor: "white" }}
      ></View>

      <View
        style={{
          width: "100%",
          alignItems: "center",
          backgroundColor: "white",
        }}
      >
        <TouchableOpacity
          onPress={() => navigation.navigate("StoreInfo")}
          style={{
            height: 40,
            width: width - 190,
            backgroundColor: "#fb5414",
            borderRadius: 39,
            justifyContent: "center",
            bottom: 3,
          }}
        >
          <Text
            style={{
              color: "white",
              fontFamily: "Helvetica-Bold",
              textAlign: "center",
              fontSize: 17,
            }}
          >
            Confirm Order
          </Text>
        </TouchableOpacity>
      </View>

      <View
        style={{ height: 3, width: width, backgroundColor: "white" }}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({});
