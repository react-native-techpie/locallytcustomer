import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import Icon from "react-native-vector-icons/FontAwesome5";

const { height, width } = Dimensions.get("window");

export default function Register({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");

  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 20,
        }}
      >
        <View
          style={{
            width: "60%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent:'flex-end'
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>
        {/* <TouchableOpacity
          style={{
            width: "40%",
            paddingLeft: 20,
            alignItems: "center",
            height: 50,
            paddingRight: 20,
            justifyContent: "center",
          }}
          onPress={() => navigation.navigate("Location")}
        >
          <View
            style={{
              width: "100%",
              alignItems: "center",
              height: 50,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <Image
              style={{ height: 30, width: 30 }}
              source={require("../LocallyTImages/location.png")}
            />
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontSize: 18,
                fontFamily: "Helvetica-Bold",
              }}
            >
              {" "}
              Pune{" "}
            </Text>
            <Image
              style={{ height: 20, width: 20 }}
              source={require("../LocallyTImages/DropDown.png")}
            />
          </View>
        </TouchableOpacity> */}
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
          flex: 1,
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              width: width,
              alignItems: "center",
            }}
          >
            <View style={{ width: "90%", marginTop: 15, padding: 0 }}>
              <Text
                style={{
                  fontFamily: "Helvetica",
                  fontSize: 19,
                  color: "#253f67",
                  marginLeft: 12,
                  lineHeight: 20,
                }}
              >
                Name
              </Text>
              <TextInput
                style={{
                  height: 45,
                  width: "100%",
                  borderWidth: 1,
                  borderRadius: 30,
                  borderColor: "#253f67",
                  backgroundColor: "white",
                  paddingLeft: 15,
                  fontFamily: "Helvetica",
                  fontSize: 16,
                }}
                placeholder="Enter your name"
              />
              <Text
                style={{
                  fontFamily: "Helvetica",
                  fontSize: 19,
                  color: "#253f67",
                  marginLeft: 12,
                  lineHeight: 20,
                  marginTop: 17,
                }}
              >
                Email ID
              </Text>
              <TextInput
                style={{
                  height: 45,
                  width: "100%",
                  borderWidth: 1,
                  borderRadius: 30,
                  borderColor: "#253f67",
                  backgroundColor: "white",
                  paddingLeft: 15,
                  fontFamily: "Helvetica",
                  fontSize: 16,
                }}
                placeholder="Enter your Email ID (optional)"
              />
              <Text
                style={{
                  fontFamily: "Helvetica",
                  fontSize: 19,
                  color: "#253f67",
                  marginLeft: 12,
                  lineHeight: 20,
                  marginTop: 17,
                }}
              >
                Enter Your Address
              </Text>
              <TextInput
                style={{
                  height: 45,
                  width: "100%",
                  borderWidth: 1,
                  borderRadius: 30,
                  borderColor: "#253f67",
                  backgroundColor: "white",
                  paddingLeft: 15,
                  fontFamily: "Helvetica",
                  fontSize: 16,
                }}
                placeholder="Flat no./Building name/Street name"
                multiline
              />
              <View
                style={{
                  borderRadius: 15,
                  marginTop: 15,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",

                  height: 45,
                  width: "100%",
                  borderWidth: 1,
                  borderRadius: 30,
                  borderColor: "#253f67",
                  backgroundColor: "white",
                }}
              >
                <TextInput
                  style={{
                    height: 40,
                    width: width - 90,
                    borderRadius: 10,
                    paddingLeft: 15,
                    fontFamily: "Helvetica",
                    fontSize: 16,
                  }}
                  placeholder="Area / LocallyT name"
                />
                <View
                  style={{
                    height: 39,
                    width: 45,
                    borderLeftWidth: 0.8,
                    borderColor: "#253f67",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Image
                    style={{ height: 25, width: 25 }}
                    source={require("../LocallyTImages/search.png")}
                  />
                </View>
              </View>
              <View
                style={{
                  width: "100%",
                  height: 50,
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 5,
                }}
              >
                {/* <Image
                style={{ height: 30, width: 30 }}
                source={require("../LocallyTImages/Profilee.png")}
              /> */}
                <Icon
                  name="compass"
                  color="#fb5414"
                  size={25}
                  style={{ marginLeft: 9 }}
                />
                <Text
                  style={{
                    fontSize: 17,
                    fontFamily: "Helvetica",
                    left: 20,
                  }}
                >
                  Use my current location
                </Text>
              </View>
              <View
                style={{
                  width: "100%",
                  // alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    height: 170,
                    width: "100%",
                    borderRadius: 15,
                    backgroundColor: "white",
                    marginTop: 0,
                    borderWidth: 1.3,
                    borderColor: "#cfe2f3",
                    flexDirection: "row",
                    elevation: 1,
                  }}
                ></View>
              </View>
            </View>
          </View>
          <View
            style={{
              height: 11,
              width: width,
              backgroundColor: "white",
              marginTop: 5,
            }}
          ></View>
        </ScrollView>
      </View>
      <View
        style={{ height: 14, width: width, backgroundColor: "white" }}
      ></View>

      <View
        style={{
          width: "100%",
          alignItems: "center",
          backgroundColor: "white",
        }}
      >
        <TouchableOpacity
          onPress={() => navigation.navigate("Home")}
          style={{
            height: 40,
            width: width - 190,
            backgroundColor: "#fb5414",
            borderRadius: 39,
            justifyContent: "center",
          }}
        >
          <Text
            style={{
              color: "white",
              fontFamily: "Helvetica-Bold",
              textAlign: "center",
              fontSize: 17,
            }}
          >
            Proceed
          </Text>
        </TouchableOpacity>
      </View>
      <View
        style={{ height: 14, width: width, backgroundColor: "white" }}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({});
