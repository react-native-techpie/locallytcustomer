import React, { useState, useRef } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import { Checkbox } from "react-native-paper";
import Onboarding from "./Onboarding/Onboarding";
import MobileNumber from "./Components/MobileNumber";

const { width, height } = Dimensions.get("window");

export default function Login({ navigation }) {
  const [email, setEmail] = useState("");
  const [checked, setChecked] = useState(true);
  
  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: "white" }}
      showsVerticalScrollIndicator={false}
    >
      <View
        style={{
          height: height,
          width: width,
          alignItems: "center",
          justifyContent: "flex-end",
          backgroundColor: "#253f67",
          // flex: 1,
          paddingBottom: 0,
        }}
      >
        <View
          style={{
            width: width,
            flex: 0.4,
            paddingTop: 0,
            backgroundColor: "transparent",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Onboarding />
        </View>
        <View
          style={{
            flex: 0.6,
            backgroundColor: "white",
            width: width,
            borderTopStartRadius: 100,
            alignItems: "center",
          }}
        >
          <View
            style={{
              width: width,
              alignItems: "center",
              marginTop: 6,
            }}
          >
            <Image
              source={require("../LocallyTImages/LocallyTLogo.png")}
              style={{ height: 95, width: 95 }}
            />
          </View>


          <MobileNumber />
          <TouchableOpacity
            disabled={email ? false : true}
            disabled={checked? false: true }
            onPress={() => navigation.navigate("Verify")}
            style={{
              height: 40,
              width: width - 190,
              backgroundColor: "#fb5414",
              borderRadius: 39,
              justifyContent: "center",
              marginTop: 25,
}}
          >
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                textAlign: "center",
                fontSize: 17,
              }}
            >
              Login
            </Text>
          </TouchableOpacity>

          <View
            style={{
              width: width - 55,
              alignItems: "flex-start",
              justifyContent: "space-between",
              flexDirection: "row",
              marginTop: 30,
              backgroundColor: "white",
              height: 70,
            }}
          >
            <Checkbox
              onChangeText={setEmail}
              color="#253f67"
              style={{ color: "red" }}
              status={checked ? "checked" : "unchecked"}
              onPress={() => {
                setChecked(!checked);
              }}
            />
            <View style={{ width: "90%" }}>
              <Text
                style={{
                  color: "#253f67",
                  lineHeight: 20,
                  fontSize: 15,
                  fontFamily: "Helvetica",
                  marginLeft: 5,
                }}
              >
                {" "}
                I have read and agreed to the LocallyT
              </Text>
              <Text
                style={{
                  color: "#253f67",
                  lineHeight: 20,
                  fontSize: 15,
                  fontFamily: "Helvetica",
                  marginLeft: 5,
                }}
              >
                {" "}
                Terms and Conditions, Privacy Policies
              </Text>
              <Text
                style={{
                  color: "#253f67",
                  lineHeight: 20,
                  fontSize: 15,
                  fontFamily: "Helvetica",
                  marginLeft: 5,
                }}
              >
                {" "}
                and User Agreement.
              </Text>
            </View>
          </View>
          {/* <View
            style={{ width: width, height: 50, backgroundColor: "white" }}
          ></View> */}
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  Container: {
    height: "100%",
    paddingTop: 31,
    width: width,
    backgroundColor: "#253f67",
  },

  SecondContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 2,
  },
  MainLogoView: {
    alignItems: "center",
    justifyContent: "center",
    width: width,
    backgroundColor: "white",
    borderTopStartRadius: 100,
    marginTop: 30,
    flex: 0.7,
    height: 440,
  },
  PhoneContainer: {
    height: 55,
    width: width - 65,
    backgroundColor: "#cfe2f3",
    borderWidth: 1,
    borderColor: "#253f67",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 2.5,
  },
  PhoneText: {
    height: 50,
    width: "100%",
    backgroundColor: "#cfe2f3",
    fontSize: 7,
    fontFamily: "Helvetica",
    borderColor: "#253f67",
    borderRadius: 20,
    justifyContent: "center",
    paddingTop: 12,
  },
  LoginButton: {
    height: 50,
    width: width - 70,
    backgroundColor: "#253f67",
    borderRadius: 25,
    marginTop: 22,
  },
  LoginText: {
    color: "white",
    fontFamily: "Helvetica-Bold",
    textAlign: "center",
    fontSize: 19,
    marginTop: 8,
  },
  NewView: {
    height: 70,
    width: width,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  NewText: {
    color: "#fb5414",
    // right: 10,
    fontFamily: "Helvetica",
    fontSize: 16,
  },
  RegisterView: {
    height: 28,
    width: 88,
    alignItems: "center",
    borderBottomWidth: 2,
    borderColor: "#253f67",
  },
  Register: {
    color: "#253f67",
    lineHeight: 27,
    fontSize: 21,
    fontFamily: "Helvetica-Bold",
  },
});
