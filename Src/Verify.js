import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  SafeAreaView,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from "react-native-confirmation-code-field";

const { height, width } = Dimensions.get("window");

const CELL_COUNT = 4;

export default function Verify({ navigation }) {
  const [value, setValue] = useState("");
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: "white" }}
      showsVerticalScrollIndicator={false}
    >
      <View
        style={{
          height: height,
          width: width,
          alignItems: "center",
          justifyContent: "flex-end",
          backgroundColor: "#253f67",
          // flex: 1,
          paddingBottom: 0,
        }}
      >
        <View
          style={{
            width: width,
            flex: 0.4,
            paddingTop: 0,
            backgroundColor: "transparent",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <View
            style={{
              width: width,
              flex: 0.3,
              paddingTop: 10,
              backgroundColor: "transparent",
              alignItems: "center",
              justifyContent: "center",
              marginTop: 40,
            }}
          >
            <Image
              source={require("../LocallyTImages/otp.png")}
              style={{ height: 120, width: 120, borderRadius: 0 }}
            />

            <Text
              style={{
                color: "white",
                marginLeft: 10,
                fontSize: 23,
                fontFamily: "Helvetica-Bold",
                textAlign: "center",
                marginTop: 5,
              }}
            >
              Verify OTP
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 0.7,
            backgroundColor: "white",
            width: width,
            borderTopStartRadius: 100,
            alignItems: "center",
          }}
        >
          <View
            style={{
              width: "60%",
              alignItems: "center",
              justifyContent: "center",
              height: 70,
              marginTop: 20,
            }}
          >
            <Text
              style={{
                color: "#253f67",
                lineHeight: 25,
                fontSize: 15,
                textAlign: "center",
                fontFamily: "Helvetica",
              }}
            >
              Enter 4 digit OTP sent to your mobile number
            </Text>
          </View>

          <SafeAreaView style={styles.root}>
            <CodeField
              ref={ref}
              {...props}
              value={value}
              onChangeText={setValue}
              cellCount={CELL_COUNT}
              rootStyle={styles.codeFieldRoot}
              keyboardType="number-pad"
              textContentType="oneTimeCode"
              placeholder="0"
              renderCell={({ index, symbol, isFocused }) => (
                <View
                  // Make sure that you pass onLayout={getCellOnLayoutHandler(index)} prop to root component of "Cell"
                  onLayout={getCellOnLayoutHandler(index)}
                  key={index}
                  style={[styles.cellRoot, isFocused && styles.focusCell]}
                >
                  <Text style={styles.cellText}>
                    {symbol || (isFocused ? <Cursor /> : null)}
                  </Text>
                </View>
              )}
            />
          </SafeAreaView>
          <View
            style={{
              height: 50,
              flexDirection: "row",
              width: width,
              alignItems: "center",
              justifyContent: "flex-end",
            }}
          >
            {/* <Text
              style={{
                right: 33,
                fontFamily: "Helvetica",
                fontSize: 19,
                color: "#B7BEBB",
              }}
            >
              Resend
            </Text> */}
            <Text
              style={{
                right: 33,
                fontSize: 20,
                color: "#253f67",
                marginLeft: 8,
                fontFamily: "Helvetica-Bold",
              }}
            >
              (60)
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate("Register")}
            style={{
              height: 40,
              width: width - 190,
              backgroundColor: "#fb5414",
              borderRadius: 39,
              justifyContent: "center",
              marginTop: 0,
            }}
          >
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                textAlign: "center",
                fontSize: 17,
              }}
            >
              Submit
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  Container: {
    height: "100%",
    paddingTop: 31,
    width: width,
    // alignItems: "center",
    // justifyContent: "center",
    backgroundColor: "#253f67",
  },
  root: { padding: 2, minHeight: 20, marginTop: 10 },
  title: { textAlign: "center", fontSize: 20 },
  codeFieldRoot: {
    marginTop: 1,
    width: width - 65,
    marginLeft: "auto",
    marginRight: "auto",
    height: 50,
  },
  cellRoot: {
    height: 50,
    width: 50,
    backgroundColor: "#cfe2f3",
    borderRadius: 16,
    alignItems: "center",
    fontSize: 20,
    textAlign: "center",
    justifyContent: "center",
  },
  cellText: {
    color: "#000",
    fontSize: 26,
    textAlign: "center",
  },
  focusCell: {
    borderBottomColor: "#007AFF",
    borderBottomWidth: 0.0,
  },
});
