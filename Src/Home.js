import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  SafeAreaView,
  TextInput,
} from "react-native";
import OnboardingHome from "./OnboardingHome/OnboardingHome";
import CouponsHeader from "./CouponsScreens/CouponsHeader";
import { Picker } from "@react-native-picker/picker";

const { height, width } = Dimensions.get("window");

export default function Home({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Pune");
  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: "#253f67",
        flex: 1,
      }}
    >
      <View
        style={{
          width: width,
          height: 75,
          backgroundColor: "transparent",
          alignItems: "center",
          flexDirection: "row",
          marginTop: 14,
        }}
      >
        <View
          style={{
            width: "42%",
            paddingLeft: 20,
            alignItems: "flex-start",
            height: 60,
            justifyContent: "flex-end",
          }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
            <Image
              style={{ height: 46, width: 46 }}
              source={require("../LocallyTImages/profile.png")}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "58%",
            alignItems: "flex-end",
            justifyContent: "flex-start",
            flexDirection: "row",
            height:60
          }}
        >
          <Image
            style={{ height: 30, width: 30, right: 2, bottom: 3 }}
            source={require("../LocallyTImages/location.png")}
          />
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  lineHeight: 17,
                }}
              >
                Delivery to
              </Text>
              <Image
                source={require("../LocallyTImages/DropDown.png")}
                style={{ height: 15, width: 15, left: 7, bottom: 2 }}
              />
            </View>
            <Text
              style={{
                color: "white",
                fontFamily: "Helvetica-Bold",
                fontSize: 12,
                lineHeight: 17,
              }}
            >
              Rose Icon, Pimple Saudagar
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
          borderTopStartRadius: 25,
          borderTopEndRadius: 25,
          height: "90%",
        }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              width: width,
              alignItems: "center",
              backgroundColor: "white",
              borderTopStartRadius: 25,
              borderTopEndRadius: 25,
            }}
          >
            <View
              style={{
                width: width,
                flex: 0.4,
                // paddingTop: 15,
                backgroundColor: "transparent",
                alignItems: "center",
                justifyContent: "center",
                marginTop:15
              }}
            >
              <OnboardingHome />
            </View>
            <View style={{ width: "90%", marginTop: 15 }}>
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: 17,
                  color: "#253f67",
                }}
              >
                Tap any of the categories to order from your stores
              </Text>
              <View
                style={{
                  width: "100%",
                  flexDirection: "row",
                  marginTop: 7,
                  justifyContent: "space-between",
                }}
              >
                <View style={{ width: "50%", alignItems: "center" }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate("MyTabs")}
                    style={{
                      height: 100,
                      width: 150,
                      borderColor: "#fb5414",
                      borderWidth: 1.1,
                      borderRadius: 10,
                      elevation: 1,
                      backgroundColor: "white",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Image
                      source={require("../LocallyTImages/grocery.png")}
                      style={{ height: 90, width: 135 }}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: 17,
                      color: "#253f67",
                      marginTop: 5,
                    }}
                  >
                    Grocery
                  </Text>
                </View>
                <View style={{ width: "50%", alignItems: "center" }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate("MyTabs")}
                    style={{
                      height: 100,
                      width: 150,
                      borderColor: "#fb5414",
                      borderWidth: 1.1,
                      borderRadius: 10,
                      elevation: 1,
                      backgroundColor: "white",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Image
                      source={require("../LocallyTImages/medicines.png")}
                      style={{ height: 82, width: 123 }}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: 17,
                      color: "#253f67",
                      marginTop: 5,
                    }}
                  >
                    Medicines
                  </Text>
                </View>
              </View>

              <View
                style={{
                  width: "100%",
                  flexDirection: "row",
                  marginTop: 7,
                  justifyContent: "space-between",
                }}
              >
                <View style={{ width: "50%", alignItems: "center" }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate("MyTabs")}
                    style={{
                      height: 100,
                      width: 150,
                      borderColor: "#fb5414",
                      borderWidth: 1.1,
                      borderRadius: 10,
                      elevation: 1,
                      backgroundColor: "white",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Image
                      source={require("../LocallyTImages/Resturant.png")}
                      style={{ height: 80, width: 127, borderRadius: 20 }}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: 17,
                      color: "#253f67",
                      marginTop: 5,
                    }}
                  >
                    Restaurant
                  </Text>
                </View>
                <View style={{ width: "50%", alignItems: "center" }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate("MyTabs")}
                    style={{
                      height: 100,
                      width: 150,
                      borderColor: "#fb5414",
                      borderWidth: 1.1,
                      borderRadius: 10,
                      elevation: 1,
                      backgroundColor: "white",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Image
                      source={require("../LocallyTImages/General.png")}
                      style={{ height: 80, width: 127 }}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: 17,
                      color: "#253f67",
                      marginTop: 5,
                    }}
                  >
                    General Stores
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                width: width,
                height: 3,
                borderWidth: 2,
                borderColor: "#cfe2f3",
                marginTop: 7,
              }}
            ></View>
            <View style={{ width: "90%" }}>
              <Text
                style={{
                  fontFamily: "Helvetica-Bold",
                  fontSize: 17,
                  color: "#253f67",
                  marginTop: 7,
                }}
              >
                Today's coupouns
              </Text>
            </View>
            <View style={{ width: "100%", height: 500 }}>
              <CouponsHeader />
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({});
