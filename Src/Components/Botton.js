import React, {useState} from 'react';
import { View, Text, TextInput, Dimensions, TouchableOpacity } from 'react-native';


const { height, width } = Dimensions.get("window");


export default function Buttoni(onpress) {
  const [email, setEmail] = useState("");
  const [checked, setChecked] = useState(true);

  return (
    <View>
      <TouchableOpacity
        disabled={email ? false : true}
        disabled={checked ? false : true}
        onPress={onpress}
        style={{
          height: 40,
          width: width - 190,
          backgroundColor: "#fb5414",
          borderRadius: 39,
          justifyContent: "center",
          marginTop: 25,
        }}
      >
        <Text
          style={{
            color: "white",
            fontFamily: "Helvetica-Bold",
            textAlign: "center",
            fontSize: 17,
          }}
        >
          Login
        </Text>
      </TouchableOpacity>
    </View>
  );
}