import React, {useState} from 'react';
import { View, Text, TextInput, Dimensions } from 'react-native';


const { height, width } = Dimensions.get("window");


export default function MobileNumber() {

const [email, setEmail] = useState("");
const [checked, setChecked] = useState(true);


    return (
      <View
        style={{
          height: 55,
          width: width - 66,
        }}
      >
        <View
          style={{
            height: 45,
            width: width - 65,
            backgroundColor: "#cfe2f3",
            borderWidth: 1,
            borderColor: "#253f67",
            borderRadius: 20,
            alignItems: "center",
            // justifyContent: "center",
            flexDirection: "row",
            marginTop: 10,
          }}
        >
          <View
            style={{
              width: "14%",
              alignItems: "center",
              justifyContent: "center",
              height: "100%",
              borderRightWidth: 1,
              borderTopStartRadius: 10,
              borderBottomStartRadius: 10,
            }}
          >
            <Text
              style={{
                color: "#253f67",
                fontFamily: "Helvetica-Bold",
                fontSize: 18,
              }}
            >
              +91
            </Text>
          </View>
          <TextInput
            placeholder="Enter 10 digit mobile number"
            keyboardType="phone-pad"
            maxLength={10}
            onChangeText={setEmail}
            style={{
              height: 41.3,
              width: "86%",
              backgroundColor: "#cfe2f3",
              borderColor: "#253f67",
              borderRadius: 20,
              paddingTop: 2.5,
              color: "#253f67",
              fontFamily: "Helvetica-Bold",
              fontSize: 14,
              paddingLeft: 10,
            }}
          />
        </View>
      </View>
    );
}