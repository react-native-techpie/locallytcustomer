import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  ImageBackground,
  TouchableOpacity,
  Modal,
  TouchableWithoutFeedback,
} from "react-native";
// import {
//   Collapse,
//   CollapseHeader,
//   CollapseBody,
//   AccordionList,
// } from "accordion-collapse-react-native";

const { height, width } = Dimensions.get("window");

export default function Profile({ navigation }) {
  const [open, setOpen] = useState(false);
  const [referOpen, setReferOpen] = useState(false);
  const [contactOpen, setContactOpen] = useState(false);

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{ height: "100%", backgroundColor: "#253f67" }}
    >
      <View style={styles.Container}>
        <ImageBackground
          source={require("../LocallyTImages/back.jpg")}
          style={{ height: "100%", width: width, position: "absolute" }}
        />
        <Modal visible={open} transparent={true}>
          <TouchableWithoutFeedback onPress={() => setOpen(false)}>
            <View
              style={{
                flex: 1,
                width: "100%",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "#000000aa",
              }}
            >
              <View
                style={{
                  height: "67%",
                  width: "85%",
                  backgroundColor: "white",
                  alignItems: "center",
                  borderRadius: 50,
                }}
              >
                <View
                  style={{
                    width: "77%",
                    height: 155,
                    paddingTop: 15,
                    paddingLeft: 0,
                    alignItems: "center",
                    borderBottomWidth: 2,
                    borderColor: "#253f67",
                  }}
                >
                  <Image
                    source={require("../LocallyTImages/Profilee.png")}
                    style={{ height: 90, width: 90 }}
                  />

                  <Text
                    style={{
                      color: "#253f67",
                      marginLeft: 5,
                      fontFamily: "Helvetica-Bold",
                      fontSize: 19,
                      marginTop: 10,
                    }}
                  >
                    {" "}
                    Hello, Rakesh{" "}
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    paddingLeft: 10,
                    flexDirection: "row",
                    marginTop: 20,
                    height: 30,
                    justifyContent: "space-between",
                    paddingRight: 20,
                    paddingLeft: 21,
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica",
                      fontSize: 15,
                      marginTop: 10,
                    }}
                  >
                    Linked Phone Number -{" "}
                  </Text>
                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica-Bold",
                      fontSize: 15,
                      marginTop: 10,
                    }}
                  >
                    9657842135{" "}
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    paddingLeft: 10,
                    flexDirection: "row",
                    marginTop: 20,
                    height: 30,
                    justifyContent: "space-between",
                    paddingRight: 20,
                    paddingLeft: 21,
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica",
                      fontSize: 15,
                      marginTop: 10,
                    }}
                  >
                    Vendor ID -{" "}
                  </Text>
                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica-Bold",
                      fontSize: 15,
                      marginTop: 10,
                    }}
                  >
                    VI00123{" "}
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    paddingLeft: 10,
                    flexDirection: "row",
                    marginTop: 20,
                    height: 30,
                    justifyContent: "space-between",
                    paddingRight: 20,
                    paddingLeft: 21,
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica",
                      fontSize: 15,
                      marginTop: 10,
                    }}
                  >
                    Business Email -{" "}
                  </Text>
                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica-Bold",
                      fontSize: 15,
                      marginTop: 10,
                    }}
                  >
                    email@gmail.com{" "}
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    paddingLeft: 10,
                    flexDirection: "row",
                    marginTop: 20,
                    height: 47,
                    justifyContent: "space-between",
                    paddingRight: 20,
                    paddingLeft: 21,
                    alignItems: "center",
                  }}
                >
                  <View style={{ width: "50%" }}>
                    <Text
                      style={{
                        color: "#253f67",
                        fontFamily: "Helvetica",
                        fontSize: 15,
                        marginTop: 10,
                        lineHeight: 22,
                      }}
                    >
                      Payments Linked Phone Number -{" "}
                    </Text>
                  </View>
                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica-Bold",
                      fontSize: 15,
                      marginTop: 10,
                    }}
                  >
                    9665321478{" "}
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    paddingLeft: 10,
                    flexDirection: "row",
                    marginTop: 20,
                    height: 30,
                    justifyContent: "space-between",
                    paddingRight: 20,
                    paddingLeft: 21,
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica",
                      fontSize: 15,
                      marginTop: 10,
                    }}
                  >
                    UPI ID -{" "}
                  </Text>

                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica-Bold",
                      fontSize: 15,
                      marginTop: 10,
                    }}
                  >
                    ABC4567{" "}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
        <View style={{ width: width, padding: 10, paddingLeft: 20 }}>
          <View
            style={{
              height: 86,
              borderRadius: 59,
              alignItems: "center",
              // justifyContent: "center",
              width: 86,
              backgroundColor: "#fb5414",
              marginTop: 30,
              marginLeft: 20,
            }}
          >
            <TouchableOpacity onPress={() => setOpen(true)}>
              <Image
                source={require("../LocallyTImages/Profilee.png")}
                style={{ height: 80, width: 80 }}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => setOpen(true)}>
            <Text
              style={{
                color: "white",
                marginLeft: 5,
                fontFamily: "Helvetica-Bold",
                fontSize: 18,
                marginTop: 10,
              }}
            >
              {" "}
              Hello, Rakesh{" "}
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            height: 1,
            width: "76%",
            borderWidth: 0.6,
            borderColor: "#fb5414",
            alignSelf: "flex-start",
          }}
        ></View>

        <View style={{ width: "90%", marginTop: 20 }}>
          <TouchableOpacity onPress={() => navigation.navigate("UpdateEmail")}>
            <View
              style={{
                width: "100%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/Email.png")}
                  style={{ height: 20, width: 32 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "50%",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    marginLeft: 5,
                    fontFamily: "Helvetica-Bold",
                    fontSize: 16,
                  }}
                >
                  Update Email ID
                </Text>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "30%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {/*  */}
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate("MyTabs")}
          >
            <View
              style={{
                width: "100%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/OrderHistory.png")}
                  style={{ height: 35, width: 36 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "50%",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    marginLeft: 5,
                    fontFamily: "Helvetica-Bold",
                    fontSize: 16,
                  }}
                >
                  Order History{" "}
                </Text>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "30%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              ></View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate("UpdateAddress")}
          >
            <View
              style={{
                width: "100%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/ChangeAdress.png")}
                  style={{ height: 40, width: 33.3 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "50%",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    marginLeft: 5,
                    fontFamily: "Helvetica-Bold",
                    fontSize: 16,
                  }}
                >
                  Update Address
                </Text>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "30%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {/* <TouchableOpacity>
                <View
                  style={{
                    height: 30,
                    width: 100,
                    borderRadius: 20,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "white",
                  }}
                >
                  <Text
                    style={{
                      color: "#253f67",
                      fontFamily: "Helvetica-Bold",
                      fontSize: 16,
                    }}
                  >
                    + Add New
                  </Text>
                </View>
              </TouchableOpacity> */}
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View
              style={{
                width: "100%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/Manage.png")}
                  style={{ height: 30, width: 30 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "50%",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    marginLeft: 5,
                    fontFamily: "Helvetica-Bold",
                    fontSize: 16,
                  }}
                >
                  Manage Favorites
                </Text>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "30%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              ></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity>
            <View
              style={{
                width: "100%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/Rating.png")}
                  style={{ height: 40, width: 37.3 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "50%",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    marginLeft: 5,
                    fontFamily: "Helvetica-Bold",
                    fontSize: 16,
                  }}
                >
                  Rate us
                </Text>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "30%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              ></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setContactOpen(true)}>
            <View
              style={{
                width: "100%",
                height: 70,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/Wcontact.png")}
                  style={{ height: 40, width: 37.3 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "50%",
                  justifyContent: "center",
                }}
              >
                <View>
                  <Text
                    style={{
                      color: "white",
                      marginLeft: 5,
                      fontFamily: "Helvetica-Bold",
                      fontSize: 16,
                    }}
                  >
                    Contact Us
                  </Text>
                  {/* <Text
                    style={{
                      color: "white",
                      marginLeft: 5,
                      fontFamily: "Helvetica-Bold",
                      fontSize: 15,
                    }}
                  >
                    abc@gmail.com
                  </Text> */}
                </View>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "30%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              ></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setReferOpen(true)}>
            <View
              style={{
                width: "100%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/referfriend.png")}
                  style={{ height: 40, width: 37.3 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "50%",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    marginLeft: 5,
                    fontFamily: "Helvetica-Bold",
                    fontSize: 16,
                  }}
                >
                  Refer Friend
                </Text>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "30%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              ></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => navigation.navigate("TermsAndConditions")}
          >
            <View
              style={{
                width: "100%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/terms.jpg")}
                  style={{ height: 40, width: 37.3 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "55%",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    marginLeft: 5,
                    fontFamily: "Helvetica-Bold",
                    fontSize: 16,
                  }}
                >
                  Terms and Conditions
                </Text>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "25%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              ></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => navigation.navigate("PrivacyPolicies")}
          >
            <View
              style={{
                width: "100%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/PrivacyPolicy.png")}
                  style={{ height: 40, width: 37.3 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "50%",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    marginLeft: 5,
                    fontFamily: "Helvetica-Bold",
                    fontSize: 16,
                  }}
                >
                  Privacy Policy
                </Text>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "30%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              ></View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate("Login")}>
            <View
              style={{
                width: "100%",
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <View
                style={{
                  height: "100%",
                  width: "20%",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../LocallyTImages/LogOut.png")}
                  style={{ height: 40, width: 37.3 }}
                />
              </View>
              <View
                style={{
                  height: "100%",
                  width: "50%",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    marginLeft: 5,
                    fontFamily: "Helvetica-Bold",
                    fontSize: 16,
                  }}
                >
                  Log Out
                </Text>
              </View>
              <View
                style={{
                  height: "100%",
                  width: "30%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              ></View>
            </View>
          </TouchableOpacity>

          <View style={{ height: 50, width: "100%" }}></View>
        </View>

        <Modal visible={contactOpen} transparent={true}>
          <TouchableWithoutFeedback onPress={() => setContactOpen(false)}>
            <View
              style={{
                flex: 1,
                width: "100%",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "#000000aa",
              }}
            >
              <View
                style={{
                  height: "20%",
                  width: "85%",
                  backgroundColor: "white",
                  alignItems: "center",
                  borderRadius: 10,
                  justifyContent: "space-around",
                  padding: 11,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    width: "90%",
                    alignItems: "center",
                    height: "15%",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Helvetica",
                      fontSize: 16,
                      color: "#253f67",
                      marginLeft: 12,
                      lineHeight: 20,
                    }}
                  >
                    Phone No.
                  </Text>
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: 17,
                      color: "#253f67",
                      marginLeft: 12,
                      lineHeight: 20,
                    }}
                  >
                    9874563214
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    width: "90%",
                    alignItems: "center",
                    height: "15%",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Helvetica",
                      fontSize: 16,
                      color: "#253f67",
                      marginLeft: 12,
                      lineHeight: 20,
                    }}
                  >
                    Email ID.
                  </Text>
                  <Text
                    style={{
                      fontFamily: "Helvetica-Bold",
                      fontSize: 17,
                      color: "#253f67",
                      marginLeft: 12,
                      lineHeight: 20,
                    }}
                  >
                    LocallyT@gmail.com
                  </Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        <Modal visible={referOpen} transparent={true}>
          <TouchableWithoutFeedback onPress={() => setReferOpen(false)}>
            <View
              style={{
                flex: 1,
                width: "100%",
                alignItems: "center",
                justifyContent: "flex-end",
                backgroundColor: "#000000aa",
              }}
            >
              <View
                style={{
                  height: "35%",
                  width: "100%",
                  backgroundColor: "white",
                  justifyContent: "space-around",
                  padding: 11,
                  marginTop: 2,
                }}
              >
                <Text
                  style={{
                    fontFamily: "Helvetica-Bold",
                    fontSize: 17,
                    color: "#253f67",
                    marginLeft: 12,
                    lineHeight: 20,
                  }}
                >
                  Share the App Link
                </Text>
                <View
                  style={{
                    width: "100%",
                    height: "15%",
                    alignItems: "center",
                    borderWidth: 1,
                    borderRadius: 5,
                    borderColor: "gray",
                    justifyContent: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "gray",
                      fontSize: 16,
                      fontFamily: "Helvetica",
                    }}
                  >
                    https:www.LocallyT.com
                  </Text>
                </View>
                <View
                  style={{
                    width: "100%",
                    height: "10%",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-around",
                  }}
                >
                  <TouchableOpacity>
                    <Image
                      source={require("../LocallyTImages/Emaill.png")}
                      style={{ height: 50, width: 50 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Image
                      source={require("../LocallyTImages/What.png")}
                      style={{ height: 80, width: 80 }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Image
                      source={require("../LocallyTImages/SMS.png")}
                      style={{ height: 55, width: 55 }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  Container: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: StatusBar.currentHeight,
  },
});
