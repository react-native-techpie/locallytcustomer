import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  SafeAreaView,
  TextInput,
} from "react-native";

const { height, width } = Dimensions.get("window");

export default function Medicines({ navigation }) {
  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: "white" }}
      showsVerticalScrollIndicator={false}
    >
      <View
        style={{
          height: height,
          width: width,
          alignItems: "center",
          justifyContent: "flex-end",
          backgroundColor: "#253f67",
          flex: 1,
          paddingBottom: 0,
        }}
      >
        <View
          style={{
            height: "89%",
            backgroundColor: "white",
            width: width,
            borderTopStartRadius: 20,
            borderTopEndRadius: 20,
            alignItems: "center",
          }}
        ></View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  Container: {
    height: "100%",
    paddingTop: 31,
    width: width,
    // alignItems: "center",
    // justifyContent: "center",
    backgroundColor: "#253f67",
  },
});
