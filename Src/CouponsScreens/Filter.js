import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from "react-native";

const { height, width } = Dimensions.get("window");

export default function Filter({ navigation }) {
  return (
    <View
      style={{
        width: width,
        alignItems: "center",
        justifyContent: "flex-end",
        backgroundColor: "#FFFF",
        paddingBottom: 0,
        flex: 1,
      }}
    >
      <ScrollView
        style={{ backgroundColor: "white" }}
        showsVerticalScrollIndicator={false}
      >
        <View style={{ width: width, alignItems: "center" }}>
          <View
            style={{
              height: 150,
              width: "90%",
              borderRadius: 15,
              backgroundColor: "white",
              elevation: 0.2,
              marginTop: 17,
              borderWidth: 2,
              borderColor: "#cfe2f3",
              alignItems: "center",
            }}
          >
            <View style={{ width: "100%", marginTop: 8, flexDirection: "row" }}>
              <View
                style={{
                  width: "35%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../../LocallyTImages/Shop.png")}
                  style={{ height: 75, width: 95 }}
                />
              </View>
              <View style={{ width: "65%" }}>
                <Text style={{ fontFamily: "Helvetica-Bold", marginTop: 6 }}>
                  By: Shop_name_1
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: 13,
                    marginTop: 6,
                  }}
                >
                  Details of the deal will appear here
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: 13,
                    marginTop: 6,
                  }}
                >
                  Details of the deal will appear here
                </Text>
              </View>
            </View>
            <View
              style={{
                width: "90%",
                marginTop: 9,
                height: 30,
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <View>
                <Text style={{ fontFamily: "Helvetica", color: "gray" }}>
                  20 left out of 50
                </Text>
              </View>
              <TouchableOpacity
                // onPress={() => navigation.navigate("Home")}
                style={{
                  height: 30,
                  width: "40%",
                  backgroundColor: "#fb5414",
                  borderRadius: 20,
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 14,
                    fontFamily: "Helvetica-Bold",
                  }}
                >
                  Grab Coupon
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              height: 150,
              width: "90%",
              borderRadius: 15,
              backgroundColor: "white",
              elevation: 0.2,
              marginTop: 20,
              borderWidth: 2,
              borderColor: "#cfe2f3",
              alignItems: "center",
            }}
          >
            <View style={{ width: "100%", marginTop: 8, flexDirection: "row" }}>
              <View
                style={{
                  width: "35%",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../../LocallyTImages/Shop.png")}
                  style={{ height: 75, width: 95 }}
                />
              </View>
              <View style={{ width: "65%" }}>
                <Text style={{ fontFamily: "Helvetica-Bold", marginTop: 6 }}>
                  By: Shop_name_1
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: 13,
                    marginTop: 6,
                  }}
                >
                  Details of the deal will appear here
                </Text>
                <Text
                  style={{
                    fontFamily: "Helvetica",
                    fontSize: 13,
                    marginTop: 6,
                  }}
                >
                  Details of the deal will appear here
                </Text>
              </View>
            </View>
            <View
              style={{
                width: "90%",
                marginTop: 9,
                height: 30,
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <View>
                <Text style={{ fontFamily: "Helvetica", color: "gray" }}>
                  20 left out of 50
                </Text>
              </View>
              <TouchableOpacity
                // onPress={() => navigation.navigate("Home")}
                style={{
                  height: 30,
                  width: "40%",
                  backgroundColor: "#fb5414",
                  borderRadius: 20,
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 14,
                    fontFamily: "Helvetica-Bold",
                  }}
                >
                  Grab Coupon
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    height: "100%",
    paddingTop: 31,
    width: width,
    backgroundColor: "#FFFF",
  },
});
