import React from "react";
// import { StatusBar } from "expo-status-bar";
import { View, Text, StyleSheet, Dimensions, StatusBar, Image } from "react-native";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { NavigationContainer } from "@react-navigation/native";
import Filter from "./Filter";
import Grocery from "./Grocery";
import Medicines from "./Medicines";
import Restaurants from "./Restaurents";

const { height, width } = Dimensions.get("window");

const Tab = createMaterialTopTabNavigator();

export default function CouponsHeader() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: "#FFFF",
            height: 65,
            justifyContent: 'center',
            width:width
        },
      }}
    >
      <Tab.Screen
        options={{
          title: ({ focused }) => (
            <View
              style={{
                height: 42,
                borderRadius: 10,
                width: 50,
                backgroundColor: focused ? "#253f67" : "white",
                justifyContent: "center",
                borderWidth: 1,
                borderColor: "#253f67",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
             <Image
                source={require("../../LocallyTImages/filter.png")}
                style={{
                  height: 40,
                  width: 40,
                  tintColor: focused ? "#FFFF" : "#253f67",
                }}
              />
            </View>
          ),
        }}
        name="Filter"
        component={Filter}
      />
      <Tab.Screen
        options={{
          title: ({ focused }) => (
            <View
              style={{
                height: 40,
                borderRadius: 17,
                width: 75,
                borderWidth: 1,
                backgroundColor: focused ? "#253f67" : "white",
                justifyContent: "center",
                        borderColor: "#253f67",
                right:6
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  color: focused ? "#FFFF" : "#253f67",
                }}
              >
                Grocery
              </Text>
            </View>
          ),
        }}
        name="Grocery"
        component={Grocery}
      />
      <Tab.Screen
        options={{
          title: ({ focused }) => (
            <View
              style={{
                height: 40,
                borderRadius: 17,
                width: 75,
                borderWidth: 1,
                backgroundColor: focused ? "#253f67" : "white",
                justifyContent: "center",
                        borderColor: "#253f67",
                right:6
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  color: focused ? "#FFFF" : "#253f67",
                }}
              >
                Medicines
              </Text>
            </View>
          ),
        }}
        name="Medicines"
        component={Medicines}
      />
      <Tab.Screen
        options={{
          title: ({ focused }) => (
            <View
              style={{
                height: 40,
                borderRadius: 17,
                width: 75,
                borderWidth: 1,
                backgroundColor: focused ? "#253f67" : "white",
                justifyContent: "center",
                        borderColor: "#253f67",
                right:6
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontFamily: "Helvetica-Bold",
                  fontSize: 12,
                  color: focused ? "#FFFF" : "#253f67",
                }}
              >
                Restaurants
              </Text>
            </View>
          ),
        }}
        name="Restaurants"
        component={Restaurants}
      />
    </Tab.Navigator>
  );
}
