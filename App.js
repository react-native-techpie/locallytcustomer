import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Login from './Src/Login';
import Register from './Src/Register';
import Verify from './Src/Verify';
import Home from './Src/Home';
import Profile from './Src/Profile';
import MyTabs from './Src/BottomTabs/MyTabs';
import Store from './Src/BottomTabs/NestedScreens/Store';
import TermsAndConditions from './Src/ProfileScreens/TermsAndConditions';
import UpdateEmail from './Src/ProfileScreens/ApdateEmail';
import UpdateAddress from './Src/ProfileScreens/UpdateAddress';

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{ headerShown: null }}
    >
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Verify" component={Verify} />
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="MyTabs" component={MyTabs} />
      <Stack.Screen name="Store" component={Store} />
      <Stack.Screen name="ThankYou" component={ThankYou} />
      <Stack.Screen name="TermsAndConditions" component={TermsAndConditions} />
      <Stack.Screen name="PrivacyPolicies" component={PrivacyPolicies} />
      <Stack.Screen name="UpdateEmail" component={UpdateEmail} />
      <Stack.Screen name="UpdateAddress" component={UpdateAddress} />
    </Stack.Navigator>
  );
}


import { useFonts } from "@use-expo/font";
import AppLoading from "expo-app-loading";
import ThankYou from './Src/BottomTabs/NestedScreens/ThankYou';
import PrivacyPolicies from './Src/ProfileScreens/PrivacyPolicies';
import MyOrders from './Src/BottomTabs/MyOrders';


export default function App() {


 let [fontsLoaded] = useFonts({
   "Helvetica-Bold": require("./assets/Fonts/Helvetica-Bold.ttf"),
   "Helvetica-Oblique": require("./assets/Fonts/Helvetica-Oblique.ttf"),
   "Helvetica-BoldOblique": require("./assets/Fonts/Helvetica-BoldOblique.ttf"),
   Helvetica: require("./assets/Fonts/Helvetica.ttf"),
 });
 if (!fontsLoaded) {
   return <AppLoading />;
 }



  return (
    
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>

    // <View style={styles.container}>
    //   <ChooseStore />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});




// import * as React from "react";
// import { Text, View, StyleSheet, Button, TextInput } from "react-native";
// import Constants from "expo-constants";
// export default function App () {
//   const [email, setEmail] = React.useState("");
//   console.log(email);
//   return (
//     <View style={styles.container}>
//       <TextInput placeholder="enter" onChangeText={setEmail} />
//       <Button
//         disabled={email ? false : true}
//         title="Button"
//         onPress={() => {
//           console.log("sdasds");
//         }}
//       />
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: "center",
//     paddingTop: Constants.statusBarHeight,
//     backgroundColor: "#ecf0f1",
//     padding: 8,
//   },
//   paragraph: {
//     margin: 24,
//     fontSize: 18,
//     fontWeight: "bold",
//     textAlign: "center",
//   },
// });













